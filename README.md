# INSTALLATION
## rod
The latest rod source files can be downloaded.
To compile it, one needs a .
Make the following changes, or run the patch
## cyrod
Run xx to generate rod.pxd or download it from here (works for xxx)

## precondition
Libraries or files listed below are necessary conditions:
1. Matplotlib
2. Numpy
3. Libncurses
4. Gtk
5. Pygtk

## Development Environment 
gcc (Debian 4.9.2-10) 4.9.2
Debian GNU/Linux 8.8 
Python 2.7.9
Cython version 0.21.1

## compilation
In file dir: ./CyRod
Command: python setup.py build
(A rod.pxd file will be generated corresponding to rod.h and menu.h, rod.h and menu.c will be modified a little bit)

A new folder named build will be created and a dynamic library rod.o will be created in the dir of ./build/lib.linux-x86_64-2.7

Then command: python and import rod.

#This is currently just for rnice

#Distributions for the other platforms will be done in the near future