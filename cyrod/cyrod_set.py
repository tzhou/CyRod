# -*- coding: utf-8 -*-
####################################################################################
####    CyRod - Python Wrapper for rod                                          ####
####    Email to nrszhou@gmail.com for any questions or comments                ####
####    Version : 1.0.0 (Feb. 2016)                                             ####
####    Project site: wait....                                                  ####
####################################################################################

import pygtk
import gtk
import numpy as np

class SetNotebookWindow:
    
    def entry_focus_out(self,widget,signal):
        
        widget.emit("activate")
    
    def set_symmetry_changed(self, widget, data):
        """ callback function for set_sym """
    
        if widget.get_active():
            self.rod_main.command_callback(None, 'set sym {0}'.format(data))
            
            
    def set_domain_changed(self, widget, datatype, variablename, posi_index = None): 
        """ callback function for set_dom """ 
        
        if datatype == "ndomain":
            self.rod_main.command_callback(None,'set dom ndomain {0} '.format(widget.get_active_text()))
            widget.set_active(int(self.rod_main.Get_Global_Variable('{0}'.format(variablename)))-1)
            for i in range(1,9):
                if i <= self.rod_main.Get_Global_Variable('{0}'.format(variablename)):
                    exec('self.matrix_{0}_HBox.set_visible(True)'.format(i))
                else:
                    exec('self.matrix_{0}_HBox.set_visible(False)'.format(i))
            for i in range(1,self.rod_main.Get_Global_Variable('NDOMAIN')+1):
                exec('self.matrix_{0}_occup_Entry.set_text(\'{1:.3f}\')'.format(i,self.rod_main.Get_Global_Variable('DOMOCCUP')[i-1]))
        elif datatype in ["equal", "coherent", "fitoccup", "fractional"]:
            self.rod_main.command_callback(None, 'set dom {0} {1} '.format(datatype, widget.get_active_text()))
            widget.set_active(self.rod_main.Get_Global_Variable('{0}'.format(variablename)))
            self.set_domain_equal_ComboBox.set_active(self.rod_main.Get_Global_Variable('DOMEQUAL'))
            if self.rod_main.Get_Global_Variable('DOMEQUAL') == 1:
                for i in range(1,self.rod_main.Get_Global_Variable('NDOMAIN')+1):
                    exec('self.matrix_{0}_occup_Entry.set_text(\'{1:.3f}\')'.format(i,self.rod_main.Get_Global_Variable('DOMOCCUP')[int('{0}'.format(i-1))]))
                for i in range(1,9):
                    exec('self.matrix_{0}_occup_Entry.set_sensitive(False)'.format(i))
            else:
                for i in range(1,9):
                    exec('self.matrix_{0}_occup_Entry.set_sensitive(True)'.format(i))
        elif datatype == 'matrix':
            for i in ["11", "12", "21", "22"]:
                try:
                    exec("self.mat{0} = float(self.matrix_{1}_{0}_Entry.get_text())".format(i, posi_index))
                except:
                    widget.set_text('{0:.3f}'.format((self.rod_main.Get_Global_Variable('{0}'.format(variablename))[posi_index-1])))
                    return
            self.rod_main.command_callback(None, 'set dom mat {0} {1} {2} {3} {4}'.format(posi_index, self.mat11, self.mat12, self.mat21, self.mat22))
            widget.set_text('{0:.3f}'.format(self.rod_main.Get_Global_Variable('{0}'.format(variablename))[posi_index-1]))
        elif datatype == 'occup':
            try:
                self.occup = float(widget.get_text())
            except ValueError:
                widget.set_text('{0:.3f}'.format(self.rod_main.Get_Global_Variable('{0}'.format(variablename))[posi_index-1]))
                return
            exec('self.rod_main.command_callback(None, \'set dom occu {0} {1} \')'.format(posi_index, self.occup))
            widget.set_text('{0:.3f}'.format(self.rod_main.Get_Global_Variable('DOMOCCUP')[posi_index-1]))
                
    
    def set_calculate_changed(self, widget, widget_type, label, variablename, typeflag = None):
        """ callback function for set_cal """ 
        
        if typeflag == 'float' and widget_type == 'entry':
            if variablename == 'bweight':
                if self.rod_main.Get_Global_Variable('NBULK') >= 1:
                    try:
                        self.rod_main.command_callback(None,'set cal bweight {0} {1}'.\
                                format(float(self.set_calculate_active_Entry.get_text()),float(self.set_calculate_bfractional_Entry.get_text())))
                    except:
                        pass
                self.set_calculate_active_Entry.set_text('{0:.3f}'.format(self.rod_main.Get_Global_Variable('L_INTERVAL')))
                self.set_calculate_bfractional_Entry.set_text('{0:.3f}'.format(self.rod_main.Get_Global_Variable('ERROR_FRACTION')))
                return
            else: # LSTART,LEND,BETA....
                try:
                    var = float(widget.get_text())
                except ValueError:
                    widget.set_text('{0:.3f}'.format((self.rod_main.Get_Global_Variable('{0}'.format(variablename)))))
                    return
        elif typeflag ==  'int' and widget_type == 'entry':
            try:
                var = int(widget.get_text())
            except ValueError:
                widget.set_text('{0}'.format(self.rod_main.Get_Global_Variable('{0}'.format(variablename))))
                return
        else: # combobox
            var = widget.get_active_text()
        if widget_type == "combobox":
            self.rod_main.command_callback(None, 'set cal {0} {1}'.format(label, widget.get_active_text()))
            widget.set_active(self.rod_main.Get_Global_Variable('{0}'.format(variablename)))
        elif widget_type == "entry" and variablename != 'bweight':
            self.rod_main.command_callback(None, 'set cal {0} {1} '.format(label, var))
            if typeflag == 'float':
                widget.set_text('{0:.3f}'.format(self.rod_main.Get_Global_Variable('{0}'.format(variablename))))
            elif typeflag == 'int':
                widget.set_text('{0}'.format(self.rod_main.Get_Global_Variable('{0}'.format(variablename))))
        
    def hide_window(self, widget, event):
        
        self.rod_main.Status_Bar_Set_ToggleButton.set_active(False)
        return True
        

    def __init__(self, rod_main):
        
        self.rod_main = rod_main 
        Set_Calculate_ScrolledWindow = gtk.ScrolledWindow()
        Set_Calculate_ScrolledWindow.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        Set_Calculate_Label = gtk.Label(" Calculate ")
        
        Set_Domain_ScrolledWindow = gtk.ScrolledWindow()
        Set_Domain_ScrolledWindow.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        Set_Domain_Label = gtk.Label(" Domain ")
        
        Set_Symmetry_ScrolledWindow = gtk.ScrolledWindow()
        Set_Symmetry_ScrolledWindow.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        Set_Symmetry_Label = gtk.Label(" Symmetry ")
        tooltips = gtk.Tooltips()
        
############################################## Notebook of calculate #############################################
        set_calculate_label = ("lstart", "lend", "structure","npoints", "atten", "beta", "lbragg", #7
                               "fractional","energy","nlayers","scale","subscale","sfraction", #6
                               "s2fraction","nsurf2","roughness","active","bfractional") #5
        set_calculate_variablename = ('LSTART','LEND','STRUCFAC','NL','ATTEN','BETA','LBRAGG','FRACFLAG',#8
                                'NENERGY','NLAYERS','SCALE','NSUBSCALE','SURFFRAC','SURF2FRAC','NSURF2',#7
                                'ROUGHMODEL','L_INTERVAL','ERROR_FRACTION')#3
        set_calculate_typeflag = ('float','float','combobox','int','float','float','float',#7
                                  'combobox','int','int','float','int','float','float','int','combobox','float','float')#11
        set_calculate_tips = (' Start value of l ','End value of l'," Use structure factors(else intensities) ",
                              ' Number of points on rod ',' Attenuation factor of X-ray beam ',' Roughness parameters beta ',
                           ' l-value of nearest Bragg peak '," Fractional rod ",' Serial number of X-ray energy ',
                           ' Number of layers in bulk unit cell ',' Scale factor of theory ',
                           ' Serial number active subscale parameter ',' Fraction of surface with 1st unit cell ',
                           ' Fraction of reconstructed surface with 2nd unit cell ',
                           ' Number atoms in structure belonging to 2nd unit cell ',"  Roughness mode ",'bweight_active'
                           ,'bweight_fractional')
        set_calculate_combobox_content = (('NO','YES'),('NO','YES'),
                                          ('Approx','Beta','Poisson','Gaussian','Linear','Cosine','Twolevel'))
        set_calculate_table = gtk.Table(9,2,homogeneous = False)
        for i in range(len(set_calculate_label)):
            if set_calculate_typeflag[i] == 'combobox':
                exec("self.set_calculate_{0}_HBox = gtk.HBox(homogeneous = False, spacing = 0)".format(set_calculate_label[i]))
                exec('self.set_calculate_{0}_label = gtk.Label(\"{0}:\")'.format(set_calculate_label[i]))
                exec('self.set_calculate_{0}_label.set_size_request(81,25)'.format(set_calculate_label[i]))
                exec('self.set_calculate_{0}_label.set_alignment(0,0.5)'.format(set_calculate_label[i]))
                exec('tooltips.set_tip(self.set_calculate_{0}_HBox,\'{1}\')'.format(set_calculate_label[i],set_calculate_tips[i-3]))
                exec("self.set_calculate_{0}_ComboBox = gtk.combo_box_new_text()".format(set_calculate_label[i]))
                exec("self.set_calculate_{0}_ComboBox.set_size_request(120,15)".format(set_calculate_label[i]))
                for j in range(len(set_calculate_combobox_content[(2,7,15).index(i)])):
                    exec("self.set_calculate_{0}_ComboBox.append_text(\"{1}\")".format(set_calculate_label[i],set_calculate_combobox_content[(2,7,15).index(i)][j]))
                exec("self.set_calculate_{0}_HBox.pack_start(self.set_calculate_{0}_label,False,False,0) ".format(set_calculate_label[i]))
                exec("self.set_calculate_{0}_HBox.pack_start(self.set_calculate_{0}_ComboBox,False,False,0) ".format(set_calculate_label[i]))
                #if i != 2: # set_cal_struc need to be modified in the future
                exec("self.set_calculate_{0}_ComboBox.connect(\"changed\",self.set_calculate_changed,\'combobox\', \'{0}\',\'{1}\')".format(set_calculate_label[i],set_calculate_variablename[i]))
            else:
                exec("self.set_calculate_{0}_HBox = gtk.HBox(homogeneous = False, spacing = 0)".format(set_calculate_label[i]))
                exec('self.set_calculate_{0}_label = gtk.Label(\" {0}:\")'.format(set_calculate_label[i]))
                exec('self.set_calculate_{0}_label.set_size_request(84,25)'.format(set_calculate_label[i]))
                exec('self.set_calculate_{0}_label.set_alignment(0,0.5)'.format(set_calculate_label[i]))
                exec('tooltips.set_tip(self.set_calculate_{0}_HBox,\'{1}\')'.format(set_calculate_label[i],set_calculate_tips[i]))
                exec("self.set_calculate_{0}_Entry = gtk.Entry()".format(set_calculate_label[i]))
                exec("self.set_calculate_{0}_Entry.set_size_request(120,20)".format(set_calculate_label[i]))
                exec("self.set_calculate_{0}_HBox.pack_start(self.set_calculate_{0}_label,False,False,0) ".format(set_calculate_label[i]))
                exec("self.set_calculate_{0}_HBox.pack_start(self.set_calculate_{0}_Entry,False,False,0) ".format(set_calculate_label[i]))
                exec('self.set_calculate_{0}_Entry.connect(\'focus-out-event\',self.entry_focus_out)'.format(set_calculate_label[i]))          
                if i <= 15:
                    exec("self.set_calculate_{0}_Entry.connect(\"activate\",self.set_calculate_changed,\'entry\', \'{0}\',\'{1}\',\'{2}\')".format(set_calculate_label[i],set_calculate_variablename[i],set_calculate_typeflag[i]))
                else:
                    exec("self.set_calculate_{0}_Entry.connect(\"activate\",self.set_calculate_changed,\'entry\', \'{0}\',\'bweight\',\'{1}\')".format(set_calculate_label[i],set_calculate_typeflag[i]))
            exec('set_calculate_table.attach(self.set_calculate_{0}_HBox,{1},{2},{3},{4})'.format(set_calculate_label[i],i%2,i%2+1,i/2,i/2+1))
        Set_Calculate_ScrolledWindow.add_with_viewport(set_calculate_table)

############################################## Notebook of domain #############################################
        set_domain_ndomains_HBox = gtk.HBox(homogeneous = False, spacing = 1)
        set_domain_ndomains_Label = gtk.Label(' ndomains  : ')
        set_domain_ndomains_Label.set_size_request(100,25)
        set_domain_ndomains_Label.set_alignment(0,0.5)
        self.set_domain_ndomains_combobox = gtk.combo_box_new_text()
        for i in range(8):
            self.set_domain_ndomains_combobox.append_text('{0}'.format(i+1))
        set_domain_ndomains_HBox.pack_start(set_domain_ndomains_Label,False,False,0)
        set_domain_ndomains_HBox.pack_start(self.set_domain_ndomains_combobox,False,False,0)
        
        set_domain_label = ("equal", "coherent", "fitoccup", "fractional")
        set_doman_variablename = ('DOMEQUAL','COHERENTDOMAINS','FITDOMOCC','ZEROFRACT')
        set_domain_tips = ('Include fractional order indices','All domains equal occupancy yes/no',\
                           'Add rotational domains coherently yes/no','Fit domain occupancy values')
        for i in range(len(set_domain_label)):
            exec("self.set_domain_{0}_HBox = gtk.HBox(homogeneous = False, spacing = 0)".format(set_domain_label[i]))
            exec('self.set_domain_{0}_label = gtk.Label(\" {0}:\")'.format(set_domain_label[i]))
            exec('self.set_domain_{0}_label.set_size_request(81,25)'.format(set_domain_label[i]))
            exec('self.set_domain_{0}_label.set_alignment(0,0.5)'.format(set_domain_label[i]))
            exec('tooltips.set_tip(self.set_domain_{0}_HBox,\'{1}\')'.format(set_domain_label[i],set_domain_tips[i]))
            exec("self.set_domain_{0}_ComboBox = gtk.combo_box_new_text()".format(set_domain_label[i]))
            exec("self.set_domain_{0}_ComboBox.set_size_request(120,20)".format(set_domain_label[i]))
            exec("self.set_domain_{0}_ComboBox.append_text(\"NO\")".format(set_domain_label[i]))
            exec("self.set_domain_{0}_ComboBox.append_text(\"YES\")".format(set_domain_label[i]))
            exec("self.set_domain_{0}_HBox.pack_start(self.set_domain_{0}_label,False,False,3) ".format(set_domain_label[i]))
            exec("self.set_domain_{0}_HBox.pack_start(self.set_domain_{0}_ComboBox,False,False,3) ".format(set_domain_label[i]))
            exec("self.set_domain_{0}_ComboBox.connect(\"changed\",self.set_domain_changed, \'{0}\',\'{1}\')".format(set_domain_label[i],set_doman_variablename[i]))
      
        self.set_domain_matrix_frame = gtk.Frame(' Matrix Settings ')
        self.set_domain_matrix_VBox = gtk.VBox(homogeneous = False, spacing = 3)
        self.set_domain_matrix_VBox.pack_start(set_domain_ndomains_HBox,False,False,0)
        for i in range(1,9):
            exec('self.matrix_{0}_HBox = gtk.HBox(homogeneous = False, spacing = 0)'.format(i))
            exec('self.matrix_label = gtk.Label(\'{0}\'.format(i))')
            exec('self.matrix_label.set_size_request(30,20)')
            exec('self.matrix_label.set_alignment(0.5,0.5)')
            exec('self.matrix_{0}_HBox.pack_start(self.matrix_label,False,False,0)'.format(i))
            for j in ['11','12','21','22','occup'] :
                exec('self.matrix_{0}_{1}_Entry = gtk.Entry()'.format(i,j))
                exec('self.matrix_{0}_{1}_Entry.set_size_request(75,20)'.format(i,j))
                exec('self.matrix_{0}_HBox.pack_start(self.matrix_{0}_{1}_Entry,False,False,0)'.format(i,j))
                exec('self.matrix_{0}_{1}_Entry.connect(\'focus-out-event\',self.entry_focus_out)'.format(i,j))
                if j == 'occup':
                    exec("self.matrix_{0}_{1}_Entry.connect(\"activate\",self.set_domain_changed, \'occup\',\'DOMOCCUP\',{0})".format(i,j))
                else:
                    exec("self.matrix_{0}_{1}_Entry.connect(\"activate\",self.set_domain_changed, \'matrix\',\'DOMMAT{1}\',{0})".format(i,j))
            exec('self.set_domain_matrix_VBox.pack_start(self.matrix_{0}_HBox,False,False,0)'.format(i))
        self.set_domain_matrix_frame.add(self.set_domain_matrix_VBox)
        self.set_domain_ndomains_combobox.connect('changed',self.set_domain_changed,'ndomain','NDOMAIN')
        tooltips.set_tip(self.set_domain_matrix_frame.get_child(),'set matrix for different domains')
#Construction of domain notebook
        set_domain_VBox = gtk.VBox(homogeneous = False, spacing = 3)
        set_domain_1_HBox = gtk.HBox(homogeneous = False, spacing = 0)
        set_domain_2_HBox = gtk.HBox(homogeneous = False, spacing = 0)
        set_domain_1_HBox.pack_start(self.set_domain_equal_HBox, False,False,0)
        set_domain_1_HBox.pack_start(self.set_domain_coherent_HBox, False,False,0)
        set_domain_2_HBox.pack_start(self.set_domain_fitoccup_HBox, False,False,0)
        set_domain_2_HBox.pack_start(self.set_domain_fractional_HBox, False,False,0)
        set_domain_VBox.pack_start(set_domain_1_HBox,False,False,0)
        set_domain_VBox.pack_start(set_domain_2_HBox,False,False,0)
        set_domain_VBox.pack_start(self.set_domain_matrix_frame,False,False,0)
        Set_Domain_ScrolledWindow.add_with_viewport(set_domain_VBox)
        
############################################## Notebook of fatomic #############################################
#notebook for set_fatomic
#refresh (read step has been done, data connection, ELEMENTS AND F_COEFF) 27/07/2017
#then the GUI need to be done here   
        
############################################## Notebook of Symmetry #############################################
        sym_list = ['P1', 'P2', 'PM', 'PG', 'CM', 'P2MM', 'P2MG', 'P2GG', 'C2MM', 'P4',\
                    'P4MM', 'P4GM', 'P3', 'P3M1', 'P31M', 'P6', 'P6MM']
        set_symmetry_table = gtk.Table(5,4,homogeneous = False)
        for sym_i in range(17):
            if sym_i == 0:
                set_symmetry_P1 = gtk.RadioButton(None,'P1')
                set_symmetry_table.attach(set_symmetry_P1,0,1,0,1)
                set_symmetry_P1.connect("toggled", self.set_symmetry_changed, "P1")
            else:
                exec("self.set_symmetry_{0} = gtk.RadioButton(set_symmetry_P1,\'{0}\')".format(sym_list[sym_i]))                
                exec("set_symmetry_table.attach(self.set_symmetry_{0},{1},{2},{3},{4})".format(sym_list[sym_i], sym_i%4, sym_i%4+1, sym_i/4, sym_i/4+1))
                exec("self.set_symmetry_{0}.connect(\'toggled\', self.set_symmetry_changed, \'{0}\')".format(sym_list[sym_i]))
        set_symmetry_VBox = gtk.VBox(homogeneous = False, spacing = 0)
        set_symmetry_VBox.pack_start(set_symmetry_table,False,False,0)
        Set_Symmetry_ScrolledWindow.add_with_viewport(set_symmetry_VBox)
        
############################################## Set_Window #############################################
        self.Set_Notebook = gtk.Notebook()
        self.Set_Notebook.set_tab_pos(gtk.POS_TOP)
        self.Set_Notebook.append_page(Set_Calculate_ScrolledWindow, Set_Calculate_Label)
        self.Set_Notebook.append_page(Set_Domain_ScrolledWindow, Set_Domain_Label)
        self.Set_Notebook.append_page(Set_Symmetry_ScrolledWindow, Set_Symmetry_Label)
        self.Set_Notebook.set_scrollable(1)
        # Window
        self.Set_Window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.Set_Window.set_border_width(10)
        self.Set_Window.add(self.Set_Notebook)
        self.Set_Window.set_title("Set")
        self.Set_Window.set_size_request(450,340)
        self.Set_Window.set_position(gtk.WIN_POS_CENTER)
        self.Set_Window.connect("delete_event", self.hide_window)
        self.Set_Window.show_all()
        #initialization
        for i in range(len(set_calculate_variablename)):
            if set_calculate_typeflag[i] != 'combobox':
                if set_calculate_typeflag[i] == 'float':
                    exec('self.set_calculate_{0}_Entry.set_text(\'{1:.3f}\')'.format(set_calculate_label[i],self.rod_main.Get_Global_Variable('{0}'.format(set_calculate_variablename[i]))))
                else: #int
                    exec('self.set_calculate_{0}_Entry.set_text(str(self.rod_main.Get_Global_Variable(\'{1}\')))'.format(set_calculate_label[i],set_calculate_variablename[i]))
            else: #combobox
                exec('self.set_calculate_{0}_ComboBox.set_active(int(self.rod_main.Get_Global_Variable(\'{1}\')))'.format(set_calculate_label[i],set_calculate_variablename[i]))
        self.set_domain_equal_ComboBox.set_active(int(self.rod_main.Get_Global_Variable('DOMEQUAL')))
        self.set_domain_fractional_ComboBox.set_active(int(self.rod_main.Get_Global_Variable('ZEROFRACT')))
        self.set_domain_coherent_ComboBox.set_active(int(self.rod_main.Get_Global_Variable('COHERENTDOMAINS')))
        self.set_domain_fitoccup_ComboBox.set_active(int(self.rod_main.Get_Global_Variable('FITDOMOCC')))
        self.set_domain_ndomains_combobox.set_active(int(self.rod_main.Get_Global_Variable('NDOMAIN'))-1)
        for i in range(1,9):
            for j in ('11','12','21','22'):
                exec('self.matrix_{0}_{1}_Entry.set_text(\'{2:.3f}\')'.format(i,j,np.array(self.rod_main.Get_Global_Variable('DOMMAT{0}'.format(j)))[i-1]))
        if i <= int(self.rod_main.Get_Global_Variable('NDOMAIN')):
            exec('self.matrix_{0}_HBox.set_visible(True)'.format(i))
        else:
            exec('self.matrix_{0}_HBox.set_visible(False)'.format(i))
        for i in range(1,9):
            exec('self.matrix_{0}_occup_Entry.set_text(\'{1:.3f}\')'.format(i,self.rod_main.Get_Global_Variable('DOMOCCUP')[i-1]))
            #exec('self.matrix_{0}_occup_Entry.set_text(str(self.rod_main.Get_Global_Variable(\'DOMOCCUP\')[{1}]))'.format(i,i-1))
#read data (set_fatomic)
        #self.cyNTYPES = Get_Global_Variable('set_fatomic_NTYPES')
        #self.cyELEMENT = Get_Global_Variable('set_fatomic_ELEMENT') 
        #self.cyF_COEFF = Get_Global_Variable('set_fatomic_F_COEFF')