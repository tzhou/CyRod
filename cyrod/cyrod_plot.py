# -*- coding: utf-8 -*-
####################################################################################
####    CyRod - Python Wrapper for rod                                          ####
####    Email to nrszhou@gmail.com for any questions or comments                ####
####    Version : 1.0.0 (Feb. 2016)                                             ####
####    Project site: wait....                                                  ####
####################################################################################

import gtk
import pygtk
from math import *
from matplotlib import use
use("GTkAgg")
from matplotlib import  patches
from matplotlib.figure import Figure
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as FigureCanvas
from mpl_toolkits.mplot3d import Axes3D
import numpy



class MyPlotWindow:
    
    def onclick (box, wedget, event):# THIS IS CURRENTLY NOT FUNCTIONING
        
        if event.x > 85 and event.x < 991:
            if event.y > 28 and event.y < 500:
                pass
    
    
    def Plot_atoms(self,flag):
        
        color_list = ('red','brown','purple','grey','orange','pink','yellow')
        #ELEMENT = self.rod_main.Get_Global_Variable('ELEMENT')  this is for legend in the future
        nsurf = self.rod_main.Get_Global_Variable('NSURF')
        TS = self.rod_main.Get_Global_Variable('TS')[:nsurf]
        XS = self.rod_main.Get_Global_Variable('XS')[:nsurf]
        YS = self.rod_main.Get_Global_Variable('YS')[:nsurf]
        ZS = self.rod_main.Get_Global_Variable('ZS')[:nsurf]
        XSFIT = self.rod_main.Get_Global_Variable('XSFIT')[:nsurf]
        YSFIT = self.rod_main.Get_Global_Variable('YSFIT')[:nsurf]
        ZSFIT = self.rod_main.Get_Global_Variable('ZSFIT')[:nsurf]
        CXS = []
        CYS = []
        CZS = []
        CXSFIT = []
        CYSFIT = []
        CZSFIT = []
        distance_array = numpy.zeros((nsurf,nsurf))
        atom_list_1 = []
        atom_list_2 = []
        for i in range(nsurf):
            coor = self.rod_main.Get_Car(XS[i],YS[i],ZS[i])
            CXS.append(coor[0])
            CYS.append(coor[1])
            CZS.append(coor[2])
            coor = self.rod_main.Get_Car(XSFIT[i],YSFIT[i],ZSFIT[i])
            CXSFIT.append(coor[0])
            CYSFIT.append(coor[1])
            CZSFIT.append(coor[2])
        
        font_axis = {'family': 'serif',
                'color':  'black',
                'weight': 'normal',
                'size': 12,
                }
                
        ax = self.Plot_atoms_Figure.add_subplot(111,projection = '3d')
        ax.set_xlabel('x',fontdict = font_axis)
        ax.set_ylabel('y',fontdict = font_axis)
        ax.set_zlabel('z',fontdict = font_axis)
        distance_limite = self.Distance_limite_Entry.get_text()
        # check the input parameters
        if distance_limite == '':
            distance_limite = 0
            self.Distance_limite_Entry.set_text('0')
        else:
            try:
                distance_limite = float(distance_limite)
            except ValueError:
                self.rod_main.Update_Status('Wrong input for bond limite','red')
                return
        bond_width = self.Bond_width_Entry.get_text()
        if bond_width == '':
            bond_width = 1
            self.Bond_width_Entry.set_text('1')
        else:
            try:
                bond_width = float(bond_width)
            except ValueError:
                self.rod_mainU('Wrong input for bond width','red')
                return
        atom_size = self.Atom_size_Entry.get_text()
        if atom_size == '':
            atom_size = 120
            self.Atom_size_Entry.set_text('120')
        else:
            try:
                atom_size = float(atom_size)
            except ValueError:
                self.rod_main.Update_Status('Wrong input for atom size','red')
                return
        atom_types = [0]
        for i in range(nsurf):
            if i+1 == len(TS) or TS[i] != TS[i+1] :
                atom_types.append(i-atom_types[-1]+1)
        if atom_types == []:
            atom_types = nsurf # only one type of atoms in the system
        atom_types.remove(0)
        
        vector_length = self.vector_length_Entry.get_text()
        if vector_length == '':
            vector_length = 1
            self.vector_length_Entry.set_text('1')
        else:
            try:
                vector_length = float(vector_length)
            except ValueError:
                self.rod_main.Update_Status('Wrong input for vector length','red')
                return
            
        line_width = self.line_width_Entry.get_text()
        if line_width == '' :
            line_width = 1
            self.line_width_Entry.set_text('1')
        else:
            try:
                line_width = float(line_width)
            except ValueError:
                self.rod_main.Update_Status('Wrong input for line width','red')
                return
        arrow_ratio = self.arrow_ratio_Entry.get_text()
        if arrow_ratio == '':
            arrow_ratio = 0.3
            self.arrow_ratio_Entry.set_text('0.3')
        else:
            try:
                arrow_ratio = float(arrow_ratio)
            except ValueError:
                self.rod_main.Update_Status('Wrong input for arow ratio','red')
                return
            
        if flag == 'MO': # 'mo'
            count = 0
            for i in atom_types:
                ax.scatter(CXS[count:count+i],CYS[count:count+i],CZS[count:count+i],c = color_list[atom_types.index(i)],marker = 'o',edgecolors = 'none', s = atom_size)
                count = count + i
            for atom_1 in range(nsurf):
                for atom_2 in range(atom_1+1,nsurf):
                    if atom_2 == nsurf:
                        break
                    if atom_1 == atom_2:
                        continue
                    else:
                        distance_array[atom_1][atom_2] = pow(((CXS[atom_1]-CXS[atom_2])**2+\
                                      (CYS[atom_1]-CYS[atom_2])**2+(CZS[atom_1]-CZS[atom_2])**2),0.5)
                        if distance_array[atom_1,atom_2] < distance_limite:
                            atom_list_1.append(atom_1)
                            atom_list_2.append(atom_2)
            for i in range(len(atom_list_1)):
                x = [CXS[atom_list_1[i]],CXS[atom_list_2[i]]]
                y = [CYS[atom_list_1[i]],CYS[atom_list_2[i]]]
                z = [CZS[atom_list_1[i]],CZS[atom_list_2[i]]]
                ax.plot(x, y, z,c = 'black',lw = bond_width)
        if flag == 'MB' or flag == 'MR': #mfit
            count = 0   # for different types of atoms and the exact qualities are saved in atom_types
            for i in atom_types:
                ax.scatter(CXSFIT[count:count+i],CYSFIT[count:count+i],CZSFIT[count:count+i],c = color_list[atom_types.index(i)],marker = 'o',edgecolors = "none", s = atom_size)
                count = count + i
            for atom_1 in range(nsurf):
                for atom_2 in range(atom_1+1,nsurf):
                    if atom_2 == 117:
                        break
                    if atom_1 == atom_2:
                        continue
                    else:
                        distance_array[atom_1][atom_2] = pow(((CXSFIT[atom_1]-CXSFIT[atom_2])**2+\
                                      (CYSFIT[atom_1]-CYSFIT[atom_2])**2+(CZSFIT[atom_1]-CZSFIT[atom_2])**2),0.5)
                        if distance_array[atom_1,atom_2] < distance_limite:
                            atom_list_1.append(atom_1)
                            atom_list_2.append(atom_2)
            for i in range(len(atom_list_1)):
                x = [CXSFIT[atom_list_1[i]],CXSFIT[atom_list_2[i]]]
                y = [CYSFIT[atom_list_1[i]],CYSFIT[atom_list_2[i]]]
                z = [CZSFIT[atom_list_1[i]],CZSFIT[atom_list_2[i]]]
                ax.plot(x, y, z,c = 'black',lw= bond_width)
            if flag == 'MB':
                ax.quiver(CXS,CYS,CZS,CXSFIT,CYSFIT,CZSFIT,color = 'blue',length = vector_length,lw = line_width,arrow_length_ratio = arrow_ratio)
        self.Plot_atoms_Canvas.draw()
        
                
    def Plot_hk(self,flag):
        
        hth = self.rod_main.Get_Global_Variable('HTH')
        kth = self.rod_main.Get_Global_Variable('KTH')
        fth = self.rod_main.Get_Global_Variable('FTH')[1]
        fdat = self.rod_main.Get_Global_Variable('FDAT')
        rlat = self.rod_main.Get_Global_Variable('RLAT')
        nth = self.rod_main.Get_Global_Variable('NTH')
        ndat = self.rod_main.Get_Global_Variable('NDAT')
        
        self.Plot_in_plane_Figure.clear()
        x = hth*rlat[0]+kth*rlat[1]*cos(rlat[5])
        y = kth*rlat[1]*sin(rlat[5])
        fmax = max(fdat.max(), fth.max())
        fth = fth/fmax * .2
        fdat = fdat/fmax * .2
        size_factor = self.size_factor_Entry.get_text()
        if size_factor == '':
            size_factor = 1/5.5
            self.size_factor_Entry.set_text('{0:.5}'.format(size_factor))
        else:
            try:
                size_factor = float(size_factor)
            except ValueError:
                self.rod_main.Update_Status('Wrong size input!','red')
                return
        font_axis = {'family': 'serif',
                'color':  'black',
                'weight': 'normal',
                'size': 12,
                }
        Plot_Axe = self.Plot_in_plane_Figure.add_axes([0.08, 0.08, 0.87, 0.87])
        if flag == 'BOTH':
            for i in range(nth):
                new_wedge = patches.Wedge((x[i],y[i]), fth[i]*size_factor, 90, 270, color="black", ec= 'None')
                Plot_Axe.add_patch(new_wedge)
            for i in range(ndat):
                new_wedge = patches.Wedge((x[i],y[i]), fdat[i]*size_factor, -90, 90, color="white", ec= 'black')
                Plot_Axe.add_patch(new_wedge)
            Plot_Axe.set_xlim(x.min()-0.1*(x.max()-x.min())-(max(fth)+max(fdat))/4, x.max()+0.1*(x.max()-x.min())+(max(fth)+max(fdat))/4)
            Plot_Axe.set_ylim(y.min()-0.1*(y.max()-y.min())-(max(fth)+max(fdat))/4, 1.1*(y.max()+0.2*(y.max()-y.min()))+(max(fth)+max(fdat))/4)
            Plot_Axe.set_aspect(1)
        elif flag == 'FTH':
            for i in range(nth):
                new_wedge = patches.Wedge((x[i],y[i]), fth[i]*size_factor, 90, 270, color="black", ec= 'None')
                Plot_Axe.add_patch(new_wedge)
            Plot_Axe.set_xlim(x.min()-0.1*(x.max()-x.min())-(max(fth)+max(fdat))/4, x.max()+0.1*(x.max()-x.min())+(max(fth)+max(fdat))/4)
            Plot_Axe.set_ylim(y.min()-0.1*(y.max()-y.min())-(max(fth)+max(fdat))/4, 1.1*(y.max()+0.2*(y.max()-y.min()))+(max(fth)+max(fdat))/4)
            Plot_Axe.set_aspect(1)
        elif flag == 'FDAT':
            for i in range(ndat):
                new_wedge = patches.Wedge((x[i],y[i]), fdat[i]*size_factor, -90, 90, color="black", ec= 'None')
                Plot_Axe.add_patch(new_wedge)
            Plot_Axe.set_xlim(x.min()-0.1*(x.max()-x.min())-(max(fth)+max(fdat))/4, x.max()+0.1*(x.max()-x.min())+(max(fth)+max(fdat))/4)
            Plot_Axe.set_ylim(y.min()-0.1*(y.max()-y.min())-(max(fth)+max(fdat))/4, 1.1*(y.max()+0.2*(y.max()-y.min()))+(max(fth)+max(fdat))/4)
            Plot_Axe.set_aspect(1)
        self.Plot_in_plane_Canvas.draw()
                
    def Plot_rod(self,flag):
        
        font_axis = {'family': 'serif',
                'color':  'black',
                'weight': 'normal',
                'size': 12,
                }
        self.Plot_out_plane_Figure.clear()
        marker_selected = self.marker_out_plane_ComboBox.get_active_text()
        size_selected = self.size_in_plane_Entry.get_text()
        color_selected = self.color_out_plane_ComboBox.get_active_text()
        if size_selected == '':
            size_selected == 25
            self.size_in_plane_Entry.set_text('25')
        else:
            try:
                size_selected = float(size_selected)
            except ValueError:
                self.rod_main.Update_Status('Wrong input for size','red')
                return
        if flag == 'Data':
            NDAT = self.rod_main.Get_Global_Variable('NDAT')
            lrod = self.rod_main.Get_Global_Variable('LDAT')[:NDAT]
            frod = self.rod_main.Get_Global_Variable('FDAT')[:NDAT]
            erod = self.rod_main.Get_Global_Variable('ERRDAT')[:NDAT]
            HROD = self.rod_main.Get_Global_Variable('HROD')
            KROD = self.rod_main.Get_Global_Variable('KROD')
            HDAT = self.rod_main.Get_Global_Variable('HDAT')
            KDAT = self.rod_main.Get_Global_Variable('KDAT')
            Plot_Axe = self.Plot_out_plane_Figure.add_subplot(111)
            Plot_Axe.set_xlabel('X',fontdict = font_axis)
            Plot_Axe.set_ylabel('Y',fontdict = font_axis)
            if numpy.max(frod)/numpy.min(frod) > 100:
                Plot_Axe.set_yscale('log')
            Plot_Axe.scatter(lrod,frod,color = color_selected,marker = marker_selected,s = size_selected)
        if flag == 'Rdata':
            NDAT = self.rod_main.Get_Global_Variable('NDAT')
            lrod = self.rod_main.Get_Global_Variable('LDAT')[:NDAT]
            frod = self.rod_main.Get_Global_Variable('FDAT')[:NDAT]
            erod = self.rod_main.Get_Global_Variable('ERRDAT')[:NDAT]  # this might be useful for the plotting
            HROD = self.rod_main.Get_Global_Variable('HROD')
            KROD = self.rod_main.Get_Global_Variable('KROD')
            HDAT = self.rod_main.Get_Global_Variable('HDAT')
            KDAT = self.rod_main.Get_Global_Variable('KDAT')
            Plot_Axe = self.Plot_out_plane_Figure.add_subplot(111)
            Plot_Axe.set_xlabel('X',fontdict = font_axis)
            Plot_Axe.set_ylabel('Y',fontdict = font_axis)
            x = []
            y = []
            for i in range(NDAT):
                if HDAT[i] == HROD and KDAT[i] == KROD:
                    x.append(lrod[i])
                    y.append(frod[i])
            if x != []:
                if max(y)/min(y) > 100:
                    Plot_Axe.set_yscale('log')
            Plot_Axe.scatter(x,y,color = color_selected,marker = marker_selected,s = size_selected)
        elif flag == 'Bulk':
            NTH = self.rod_main.Get_Global_Variable('NTH')
            NDAT = self.rod_main.Get_Global_Variable('NDAT')
            LTH = self.rod_main.Get_Global_Variable('LTH')[:NTH]
            FTH_0 = self.rod_main.Get_Global_Variable('FTH')[0][:NTH]
            Plot_Axe = self.Plot_out_plane_Figure.add_subplot(111)
            Plot_Axe.set_xlabel('X',fontdict = font_axis)
            Plot_Axe.set_ylabel('Y',fontdict = font_axis)
            if numpy.max(FTH_0)/numpy.min(FTH_0) > 100:
                Plot_Axe.set_yscale('log')
            Plot_Axe.scatter(LTH,FTH_0,color = color_selected,marker = marker_selected,s = size_selected)
        elif flag == 'Surf':
            NTH = self.rod_main.Get_Global_Variable('NTH')
            NDAT = self.rod_main.Get_Global_Variable('NDAT')
            LTH = self.rod_main.Get_Global_Variable('LTH')[:NTH]
            FTH_1 = self.rod_main.Get_Global_Variable('FTH')[1][:NTH]
            
            Plot_Axe = self.Plot_out_plane_Figure.add_subplot(111)
            Plot_Axe.set_xlabel('X',fontdict = font_axis)
            Plot_Axe.set_ylabel('Y',fontdict = font_axis)
            
            if numpy.max(FTH_1)/numpy.min(FTH_1) > 100:
                Plot_Axe.set_yscale('log')
            Plot_Axe.scatter(LTH,FTH_1,color = color_selected,marker = marker_selected,s = size_selected)
        self.Plot_out_plane_Canvas.draw()
  
    
    def Plot_atom_callback(self,widget,data = None):
        
        if data == None:
            self.Plot_atoms(self.Plot_atoms_flag)
        elif widget.get_active:
            self.Plot_atoms(data)
            self.Plot_atoms_flag = data
            
            
    def Plot_hk_callback(self,widget,data = None):
        
        if data == None:
            self.Plot_hk(self.Plot_hk_flag)
        elif widget.get_active():
            self.Plot_hk(data)
            self.Plot_hk_flag = data
        
    def Plot_rod_callback(self,widget,data = None):
        
        if data == None:
            self.Plot_rod(self.Plot_rod_flag)
        elif widget.get_active():
            self.Plot_rod(data)
            self.Plot_rod_flag = data
        
    def hide_window(self, widget,event):
        
        self.Plot_Window.hide()
        self.rod_main.Status_Bar_Plot_ToggleButton.set_active(False)
        return True
        
        
    def __init__(self,rod_main):
        
        self.rod_main = rod_main  # main window, used to communicate
        tooltips = gtk.Tooltips()
        self.Plot_atoms_Figure = Figure()
        self.Plot_in_plane_Figure = Figure()
        self.Plot_out_plane_Figure = Figure()
        
        Plot_atoms_struc_ScrolledWindow = gtk.ScrolledWindow()
        Plot_atoms_struc_ScrolledWindow.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        Plot_atoms_struc_Label = gtk.Label(" Atoms Config ")
        
        Plot_in_plane_ScrolledWindow = gtk.ScrolledWindow()
        Plot_in_plane_ScrolledWindow.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        Plot_in_plane_Label = gtk.Label(" In Plane ")
        
        Plot_out_plane_ScrolledWindow = gtk.ScrolledWindow()
        Plot_out_plane_ScrolledWindow.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        Plot_out_plane_Label = gtk.Label(" Out of Plane ")
        
################################# Plot out plane  #################################   
        
        color_list = ('blue','black','red','yellow','brown','purple','pink','orange','grey')
        self.color_out_plane_ComboBox = gtk.combo_box_new_text()
        for color in color_list:
            self.color_out_plane_ComboBox.append_text(color)
        self.color_out_plane_ComboBox.set_active(0)
        color_out_plane_Label = gtk.Label('    Color:')
        self.color_out_plane_ComboBox.connect('changed',self.Plot_rod_callback)
        
        marker_tuple = ('.',',','o','v','^','>','1','2','3','4','8','s','p','*')
        self.marker_out_plane_ComboBox = gtk.combo_box_new_text()
        for marker in marker_tuple:
            self.marker_out_plane_ComboBox.append_text(marker)
        self.marker_out_plane_ComboBox.set_active(0)
        self.marker_out_plane_ComboBox.connect('changed',self.Plot_rod_callback)
        marker_out_plane_Label = gtk.Label('    Marker:')
        
        
        Plot_in_plane_fth_RadioButton = gtk.RadioButton(None,'FTH')
        tooltips.set_tip(Plot_in_plane_fth_RadioButton,'Theoretical Factor')
        Plot_in_plane_fth_RadioButton.connect('toggled',self.Plot_hk_callback,'FTH')
        Plot_in_plane_fdat_RadioButton = gtk.RadioButton(Plot_in_plane_fth_RadioButton,'FDAT')
        tooltips.set_tip(Plot_in_plane_fdat_RadioButton,'Data Factor')
        Plot_in_plane_fdat_RadioButton.connect('toggled',self.Plot_hk_callback,'FDAT')
        
        Plot_out_plane_dat_RadioButton = gtk.RadioButton(None,'DATA')
        Plot_out_plane_dat_RadioButton.connect('toggled',self.Plot_rod_callback,'Data')
        Plot_out_plane_rdat_RadioButton = gtk.RadioButton(Plot_out_plane_dat_RadioButton,'RDAT')
        Plot_out_plane_rdat_RadioButton.connect('toggled',self.Plot_rod_callback,'Rdata')
        Plot_out_plane_bulk_RadioButton = gtk.RadioButton( Plot_out_plane_rdat_RadioButton,'BULK')
        Plot_out_plane_bulk_RadioButton.connect('toggled',self.Plot_rod_callback,'Bulk')
        Plot_out_plane_surf_RadioButton = gtk.RadioButton(Plot_out_plane_bulk_RadioButton,'SURF')
        Plot_out_plane_surf_RadioButton.connect('toggled',self.Plot_rod_callback,'Surf')
        self.size_in_plane_Label = gtk.Label('    size:')
        self.size_in_plane_Entry = gtk.Entry()
        self.size_in_plane_Entry.connect('activate',self.Plot_rod_callback)
        self.size_in_plane_Entry.set_text('80')
        self.size_in_plane_Entry.set_size_request(65,25)
        
        Plot_out_plane_HBox = gtk.HBox(homogeneous = False, spacing = 5)
        Plot_out_plane_HBox.pack_start(Plot_out_plane_dat_RadioButton,False,False,0)
        Plot_out_plane_HBox.pack_start(Plot_out_plane_rdat_RadioButton,False,False,0)
        Plot_out_plane_HBox.pack_start(Plot_out_plane_bulk_RadioButton,False,False,0)
        Plot_out_plane_HBox.pack_start(Plot_out_plane_surf_RadioButton,False,False,0)
        Plot_out_plane_HBox.pack_start(marker_out_plane_Label,False,False,0)
        Plot_out_plane_HBox.pack_start(self.marker_out_plane_ComboBox,False,False,0)
        Plot_out_plane_HBox.pack_start(color_out_plane_Label,False,False,0)
        Plot_out_plane_HBox.pack_start(self.color_out_plane_ComboBox,False,False,0)
        Plot_out_plane_HBox.pack_start(self.size_in_plane_Label,False,False,0)
        Plot_out_plane_HBox.pack_start(self.size_in_plane_Entry,False,False,0)
        
        Plot_out_plane_VBox = gtk.VBox(homogeneous = False,spacing = 5) 
        self.Plot_out_plane_Canvas = FigureCanvas(self.Plot_out_plane_Figure)  
        self.Plot_out_plane_Canvas.connect('button-press-event', self.onclick)
        Plot_out_plane_VBox.pack_start(self.Plot_out_plane_Canvas) 
        Plot_out_plane_VBox.pack_start(Plot_out_plane_HBox,False,False,0)
        
#################################### Plot in plane  ####################################      
  
        Plot_in_plane_fth_RadioButton = gtk.RadioButton(None,'FTH')
        tooltips.set_tip(Plot_in_plane_fth_RadioButton,'Theoretical Factor')
        Plot_in_plane_fth_RadioButton.connect('toggled',self.Plot_hk_callback,'FTH')
        Plot_in_plane_fdat_RadioButton = gtk.RadioButton(Plot_in_plane_fth_RadioButton,'FDAT')
        tooltips.set_tip(Plot_in_plane_fdat_RadioButton,'Data Factor')
        Plot_in_plane_fdat_RadioButton.connect('toggled',self.Plot_hk_callback,'FDAT')
        Plot_in_plane_both_RadioButton = gtk.RadioButton(Plot_in_plane_fdat_RadioButton,'BOTH')
        tooltips.set_tip(Plot_in_plane_both_RadioButton,'Data Factor')
        Plot_in_plane_both_RadioButton.connect('toggled',self.Plot_hk_callback,'BOTH')
        size_factor_Label = gtk.Label('    size factor:')
        self.size_factor_Entry = gtk.Entry()
        self.size_factor_Entry.connect('activate',self.Plot_hk_callback)
        self.size_factor_Entry.set_size_request(100,25)
        
        Plot_in_plane_HBox = gtk.HBox(homogeneous = False, spacing = 5)
        Plot_in_plane_HBox.pack_start(Plot_in_plane_fth_RadioButton,False,False,0)
        Plot_in_plane_HBox.pack_start(Plot_in_plane_fdat_RadioButton,False,False,0)
        Plot_in_plane_HBox.pack_start(Plot_in_plane_both_RadioButton,False,False,0)
        Plot_in_plane_HBox.pack_start(size_factor_Label,False,False,0)
        Plot_in_plane_HBox.pack_start(self.size_factor_Entry,False,False,0)
        
        Plot_in_plane_VBox = gtk.VBox(homogeneous = False,spacing = 5) 
        self.Plot_in_plane_Canvas = FigureCanvas(self.Plot_in_plane_Figure)  
        self.Plot_in_plane_Canvas.connect('button-press-event', self.onclick)
        Plot_in_plane_VBox.pack_start(self.Plot_in_plane_Canvas) 
        Plot_in_plane_VBox.pack_start(Plot_in_plane_HBox,False,False,0)
        
####################################### Plot Atoms Config #####################################
        Plot_atoms_struc_mo_RadioButton =gtk.RadioButton(None,'Original')
        tooltips.set_tip(Plot_atoms_struc_mo_RadioButton,'Original Model')
        Plot_atoms_struc_mo_RadioButton.connect('toggled',self.Plot_atom_callback,'MO')
        Plot_atoms_struc_mfit_RadioButton = gtk.RadioButton(Plot_atoms_struc_mo_RadioButton,'Refined')
        tooltips.set_tip(Plot_atoms_struc_mfit_RadioButton,'Refined Model')
        Plot_atoms_struc_mfit_RadioButton.connect('toggled',self.Plot_atom_callback,'MR')
        Plot_atoms_struc_both_RadioButton = gtk.RadioButton(Plot_atoms_struc_mfit_RadioButton,'Both')
        tooltips.set_tip(Plot_atoms_struc_both_RadioButton,'Refined Model and DISP')
        Plot_atoms_struc_both_RadioButton.connect('toggled',self.Plot_atom_callback,'MB')
        
        Distance_limite_Label = gtk.Label('    Bond lim:')
        tooltips.set_tip(Distance_limite_Label,' Set the bond length limite ')
        self.Distance_limite_Entry = gtk.Entry()
        self.Distance_limite_Entry.connect('activate',self.Plot_atom_callback)
        self.Distance_limite_Entry.set_size_request(60,25)
        Bond_width_Label = gtk.Label('    Bw:')
        tooltips.set_tip(Bond_width_Label,' Set the bond width ')
        self.Bond_width_Entry = gtk.Entry()
        self.Bond_width_Entry.connect('activate',self.Plot_atom_callback)
        self.Bond_width_Entry.set_size_request(60,25)
        Atom_size_Label = gtk.Label('    Atom Size:')
        tooltips.set_tip(Atom_size_Label,' Set the atom size ')
        self.Atom_size_Entry = gtk.Entry()
        self.Atom_size_Entry.connect('activate',self.Plot_atom_callback)
        self.Atom_size_Entry.set_size_request(60,25)
        
        vector_length_Label = gtk.Label('    vector size:')
        tooltips.set_tip(vector_length_Label,'Set vector length for displacement vectors')
        self.vector_length_Entry = gtk.Entry()
        self.vector_length_Entry.connect('activate',self.Plot_atom_callback)
        self.vector_length_Entry.set_size_request(65,25)
        line_width_Label = gtk.Label('    Lw:')
        tooltips.set_tip(line_width_Label,'Set line width for displacement vectors')
        self.line_width_Entry = gtk.Entry()
        self.line_width_Entry.connect('activate',self.Plot_atom_callback)
        self.line_width_Entry.set_size_request(65,25)
        arrow_ratio_Label = gtk.Label('    Arrow:')
        tooltips.set_tip(arrow_ratio_Label,'Set arrow factor for displacement vectors')
        self.arrow_ratio_Entry = gtk.Entry()
        self.arrow_ratio_Entry.connect('activate',self.Plot_atom_callback)
        self.arrow_ratio_Entry.set_size_request(65,25)
        
        Plot_atoms_HBox = gtk.HBox(homogeneous = False, spacing = 5)
        Plot_atoms_HBox.pack_start(Plot_atoms_struc_mo_RadioButton,False,False,0)
        Plot_atoms_HBox.pack_start(Plot_atoms_struc_mfit_RadioButton,False,False,0)
        Plot_atoms_HBox.pack_start(Plot_atoms_struc_both_RadioButton,False,False,0)
        Plot_atoms_HBox.pack_start(Distance_limite_Label,False,False,0)
        Plot_atoms_HBox.pack_start(self.Distance_limite_Entry,False,False,0)
        Plot_atoms_HBox.pack_start(Bond_width_Label,False,False,0)
        Plot_atoms_HBox.pack_start(self.Bond_width_Entry,False,False,0)
        Plot_atoms_HBox.pack_start(Atom_size_Label,False,False,0)
        Plot_atoms_HBox.pack_start(self.Atom_size_Entry,False,False,0)
        Plot_atoms_HBox.pack_start(vector_length_Label,False,False,0)
        Plot_atoms_HBox.pack_start(self.vector_length_Entry,False,False,0)
        Plot_atoms_HBox.pack_start(line_width_Label,False,False,0)
        Plot_atoms_HBox.pack_start(self.line_width_Entry,False,False,0)
        Plot_atoms_HBox.pack_start(arrow_ratio_Label,False,False,0)
        Plot_atoms_HBox.pack_start(self.arrow_ratio_Entry,False,False,0)
        
        Plot_atoms_VBox = gtk.VBox(homogeneous = False, spacing = 5)
        self.Plot_atoms_Canvas = FigureCanvas(self.Plot_atoms_Figure)
        Plot_atoms_VBox.pack_start(self.Plot_atoms_Canvas)
        Plot_atoms_VBox.pack_start(Plot_atoms_HBox,False,False,0)
        
        Plot_in_plane_ScrolledWindow.add_with_viewport(Plot_in_plane_VBox)
        Plot_atoms_struc_ScrolledWindow.add_with_viewport(Plot_atoms_VBox)
        Plot_out_plane_ScrolledWindow.add_with_viewport(Plot_out_plane_VBox)
        
        #try to initialize Figures
        self.Plot_atoms_flag = 'MO'
        self.Plot_rod_flag = 'Rdata'
        self.Plot_hk_flag = 'FTH'
        try:
            self.Plot_atoms('MO')
            self.Plot_rod('Rdata')
            self.Plot_hk('FTH')
        except:
            pass # not ready for auto plotting (no plot data )
            
######################################### Plot Window ######################################
        self.Plot_Notebook = gtk.Notebook()
        self.Plot_Notebook.append_page(Plot_atoms_struc_ScrolledWindow,Plot_atoms_struc_Label)
        self.Plot_Notebook.append_page(Plot_in_plane_ScrolledWindow,Plot_in_plane_Label)
        self.Plot_Notebook.append_page(Plot_out_plane_ScrolledWindow,Plot_out_plane_Label)
        self.Plot_Window = gtk.Window()
        self.Plot_Window.set_default_size(1050,600)
        self.Plot_Window.add(self.Plot_Notebook)
        self.Plot_Window.set_title("PLOT")
        self.Plot_Window.connect("delete_event", self.hide_window)
        self.Plot_Window.show_all()
