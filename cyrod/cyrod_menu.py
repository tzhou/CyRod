# -*- coding: utf-8 -*-
####################################################################################
####    CyRod - Python Wrapper for rod                                          ####
####    Email to nrszhou@gmail.com for any questions or comments                ####
####    Version : 1.0.0 (Feb. 2016)                                             ####
####    Project site: wait....                                                  ####
####################################################################################
import gtk
import pygtk
import os

menu = {
"main_menu" :
[
["read", "Read data/model files"],
["list", "List data/model"],
["reset", "Reset all parameters"],
["calculate", "Calculate structure factors"],
["plot", "Goto plotting menu"],
["set", "Set parameters"],
["fit", "Fit experimental structure factors"],
["fitplus", "Extensive asa fit"],
#["energy", "Goto lattice energy menu"], # left to the future
#["refl", "Goto reflectivity menu"],
#[":", "Execute an operating system command"],
#["quit", "Quit program"]
]
,
"read_menu" :
[
["data", "Structure factor data"],
["surface", "Coordinates of surface atoms"],
["bulk", "Coordinates of bulk atoms"],
["fit", "File with fit model of surface atoms"],
["molecule", "File with coord. of surface molecule n"],
["parameters", "File with fit parameters (macro)"],
["fatomic", "File with atomic scatt. factors (macro)"],
["macro", "Simply a macro file (macro)"],
#["return", "Return to main menu"]
]
,
'llist_menu' :
[
["bulk", "Bulk contribution"],
["surface", "Surface contribution"],
["molecules", "Molecule(s) contribution"],
["sum", "Interf. sum of bulk, surface & molecules"],
["all", "Above three values"],
["data", "Structure factor data"],
["smodel", "Surface model"],
["bmodel", "Bulk model"],
["fit", "Fitting model for surface structure"],
["mmodel", "Molecule model in fractional format"],
["compare", "Comparison between data and theory"],
["rodcompare", "Data-theory comparison for selected rod"],
["parameters", "Values of fit parameters"],
["fatomic", "Coefficients atomic scattering factors"],
["symmetry", "Symmetry-equivalent reflections of data"],
["bonds", "Bond lengths of surface structure"],
["zdensity", "Z-proj. electron density (plot first!)"],
#["return", "Return to main menu"]
]
,
'calc_menu' :
[
["rod", "Calculate rod profile"],
["range", "Calculate f's for range of h and k"],
["qrange", "Calculate f's within q-max"],
["data", "Calculate f's for all data points"],
["distance", "Calculate the distance between two atoms"],
["angle", "Calculate bond angle between three atoms"],
["roughness", "Calculate roughness in atomic layers"],
#["return", "Return to main menu"]
]
,
'set_menu' :
[
["calculate","Parameters for rod calculation"],
["parameters","Values of fit parameters"],
["domain","Parameters describing the domains"],
#["plot","Plotting parameters"],
["symmetry","Plane group symmetry of model"],
#["fatomic","Atomic scattering factors"],
#["help","Display menu"],#we might not need help option as we have help menu alredi
#["return","Return to main menu"]
]
,
'energy_menu':
[
["alpha","Set bond length deformation par."],
["beta","Set bond angle deformation par."],
["radius","Set atomic radius (angstrom)"],
["equangle","Set equilibrium bond angle"],
["keating","Use Keating potential"],
["lennard","Use Lennard-Jones potential"],
["include","Include atom in energy calculation"],
["exclude","Exclude atom in energy calculation"],
["remove","Remove a bond from calculation"],
["addbond","Add bond to calculation"],
["findbonds","Find all bonds between atoms"],
["chisqr","Include energy in chi^2 minimization"],
["energy","Calculate lattice energy"],
["minimize","Minimize lattice energy"],
["list","List parameter values"],
["bondlist","List bonds between atoms"],
["anglelist", "List angles between atoms"],
["lengthlist","List length of all included bonds"],
["help","Display menu"],
["return","Return to main menu"],
]
,
'set_calculate_menu' :
[
["structure","Use structure factors or intensities"],
["lstart","Start value of l"],
["lend","End value of l"],
["npoints","Number of points on rod"],
["atten","Attenuation factor of beam"],
["beta","Roughness parameter beta"],
["lbragg","l-value of nearest Bragg peak"],
["fractional","Fractional rod calculation yes/no"],
["energy","Serial number of X-ray energy"],
["nlayers","Number of layers in bulk unit cell"],
["scale","Scale factor of theory"],
["subscale","Serial number active subscale parameter"],
["sfraction","Fraction of surface with 1st unit cell"],
["s2fraction","Fraction of surface with 2nd unit cell"],
["nsurf2","Number of atoms in 2nd unit cell"],
["roughness","Roughness mode calculation"],
["bweight"  ,"Decrease weight near Bragg peaks"],
["profile","Top layer occupancy profile yes/no"],
["list","List parameters"],
#["help","Display menu"],#we might not need help option as we have help menu alredi
#["return","Return to main menu"]
]
,
'set_calculate_roughness_menu' :
[
["approx","Approximated beta model"],
	#"exact",   1, i_exact, 	"Exact beta model (BULK ONLY!)",
["beta","Numerical beta model"],
["poisson","Poisson model"],
["gaussian","Gaussian model"],
["linear","Linear model"],
["cosine","Cosine model"],
["twolevel","Two-level model"],
#["help","Display menu"],#we might not need help option as we have help menu alredi
#["return","Return to last menu"]
]
,
'set_parameters_menu' : 
[
["scale","Scale factor of theory"],
["beta","Roughness parameter beta"],
["surffrac","Reconstructed-surface fraction"],
["displace","Value of displacement parameter"],
["b1","Value of parallel Debye-Waller parameter"],
["b2","Value of perpend. Debye-Waller parameter"],
["occupancy","Value of occupancy parameter"],
["subscale","Value of subscale parameter"],
["molpar","Position/rotation parameters of molecule n"],
["list","List parameters"],
#["help","Display menu"],#we might not need help option as we have help menu alredi
#["return","Return to main menu"]
]
,
'set_domain_menu' :
[
["ndomains","Number of rotational surface domains"],
["matrix","Matrix elements of domain n"],
["fractional","Include fractional coordinates yes/no"],
["equal","All domains equal occupancy yes/no"],
["occupancy","Set occupancy parameters of domain n"],
["coherent","Add rotational domains coherently yes/no"],
["fitoccup","Include domain occupancy in fit yes/no"],
["list","List parameters"],
#["help","Display menu"],#we might not need help option as we have help menu alredi
#["return","Return to main menu"]
]
,
'set_plot_menu' : 
[
["size","Size of circles in plot of f's"],
["threshold","Plotting threshold for f's"],
["radius","Radius of atom in plot of model"],
["xmincont","Lower bound on x value in patterson"],
["xmaxcont","Upper bound on x value in patterson"],
["nxcontour","Number of steps along x in patterson"],
["ymincont","Lower bound on y value in patterson"],
["ymaxcont","Upper bound on y value in patterson"],
["nycontour","Number of steps along y in patterson"],
["minlevel","Minimum contour level in patterson"],
["maxlevel","Maximum contour level in patterson"],
["nlevel","Number of contour levels in patterson"],
["list","List parameters"],
#["help","Display menu"],#we might not need help option as we have help menu alredi
#["return","Return to main menu"]
]
,
'set_symmetry_menu' : 
[
["p1","Plane group no. 1"],
["p2","Plane group no. 2"],
["pm","Plane group no. 3"],
["pg","Plane group no. 4"],
["cm","Plane group no. 5"],
["p2mm","Plane group no. 6"],
["p2mg","Plane group no. 7"],
["p2gg","Plane group no. 8"],
["c2mm","Plane group no. 9"],
["p4","Plane group no. 10"],
["p4mm","Plane group no. 11"],
["p4gm","Plane group no. 12"],
["p3","Plane group no. 13"],
["p3m1","Plane group no. 14"],
["p31m","Plane group no. 15"],
["p6","Plane group no. 16"],
["p6mm","Plane group no. 17"],
["list","List current plane group"],
["help","Display menu"],
#["return","Return to main menu"]
]
,
'set_fatomic_menu' : 
[
["fatomic","Atomic scattering factor f0"],
["dispersion","Dispersion corrections f1 and f2"],
["list","List scattering factors"],
["help","Display menu"],
#["return","Return to main menu"]
]
,
"fit_menu" :
[
["value","Set parameter value"],
["lower","Set lower parameter limit"],
["upper","Set upper parameter limit"],
["fix","Fix a parameter value"],
["free","Make parameter free"],
["center","Center parameter within range"],
["afix","All parameters fixed"],
["afree","All parameters free"],
["acenter","Center all free parameters"],
["control","Set control parameters"],
["list","List parameter values"],
["help","Display menu"],
["run","Start fit (Levenberg Marquardt)"],
["asa","Start fit (adaptive simulated annealing)"],
#["return","Return without fitting"]
]
,
"set_controlpars_menu":
[
["covariance","Use covariance matrix for error estimate"],
["chisqr","Use chi^2 for error estimate"],
["itermax","Max. number of iterations (LM fit)"],
["conv","chi^2 convergence criterion"],
#["open","Open fit summary file"],
#["close","Close fit summary file"],
#ifdef ASA
#["anneal","ASA: Temperature anneal scale"],
#["limit","ASA: Limit in number of acceptances"],
#["ratio","ASA: Temperature ratio scale"],
#["cost","ASA: Cost parameter scale"],
#["reanneal","ASA: Reannealing interval"],
#["nprint","ASA: printing inverval"],
#["userinit","ASA: user initial values (yes/no)"],
#endif /* ASA */
["list","List parameters"],
#["help","This list"],
#["return","Return to main menu"]
]
}

def get_menu(name):
    return menu[name]
    
def command_parser(items,rod_main,pass_value = None):
    
    len0 = len(items[0])
    wrong_command = True
    for main_menu_item in menu['main_menu']:
        if items[0] == main_menu_item[0][:len0]:
            wrong_command = False
            break
    if wrong_command:
        return 0, 'Unkown command: {0}'.format(items[0]),'red'
    count = 0
    for i in range(len(items)):
        if items[count] == 'return'[:len(items[count])] and items[count] != 're':
            items.remove(items[count])
            count -= 1
        count += 1
    if items[0] == 'read'[0:len0]:
        len1 = len(items[1])
        file_type_list = ('data'[:len1],'surface'[:len1],'fit'[:len1],'bulk'[:len1],'molecule'[:len1])
        if len(items) == 3: # infinite loop guard
            if items[1] in ("parameters"[:len1], "fatomic"[:len1], "macro"[:len1]):
                if items[1][0] == "p" and ".par" not in items[2]:
                    items[2] += '.par'
                elif items[1][0] == "f" and ".fat" not in items[2]:
                    items[2] += '.fat'
                elif items[1][0] == "m" and ".mac" not in items[2]:
                    items[2] += '.mac'
                if not check_file(rod_main,items[2].split('.')[1],items[2].split('.')[0]): # this is to check the exsistance of the file
                    return 0, 'No such file found: {0}'.format(items[2]),'red'
                mac_file = open(items[2])
                newline = mac_file.readline()
                mac_cmd = ""
                while(newline != ""):
                    if newline[0] == "!":
                        pass
                    else:
                        newitems = newline.split()
                        for key in menu['main_menu']:
                            if len(key[0]) >= len(newitems[0]):
                                if newitems[0] == key[0][:len(newitems[0])]:
                                    if mac_cmd != "":
                                        rod_main.command_callback(None, mac_cmd)
                                        mac_cmd = ""
                                        break
                        mac_cmd += newline.replace("\n"," ")
                    newline = mac_file.readline()
                rod_main.command_callback(None, mac_cmd)
                return 0, 'reading par','green'
            elif items[1] in file_type_list:
                extension = ('dat','sur','fit','bul','molecule')[file_type_list.index(items[1])]
                if extension == 'fit':
                    rod_main.valid_flag['Fit'] = 1 # this is to make sure that the par file is readed after fit files, however, it cannot 100% provent it from happening need to be modified
                if not check_file(rod_main,extension,items[2].split('.')[0]):
                    return 0, 'No such file found: {0}.{1}'.format(items[2],extension),'red'
                return 1,'reading {0} files'.format(items[1]),'green'
        rod_main.CommandEntry.set_text('read ')
        rod_main.CommandEntry.set_position(-1)
        return 0, 'Unknown file type: {0}'.format(items[1]),'red'  # wrong command receiver
                
    elif items[0] == "reset"[:len0]: 
        if len(items) == 1:
            return 1, 'All parameters have been set to default values','orange'
        else:
            return 0, 'Too many commands', 'red'
            
    elif items[0] == "fit"[:len0]:
        if rod_main.Get_Global_Variable('NDAT') < 1:
            return 0,'No data read in','red'
        elif rod_main.Get_Global_Variable('NSURF')+rod_main.Get_Global_Variable('NBULK') < 1:
            return 0,'No model read in','red'
        mac_cmd = 'fit '
        len1 = len(items[1])
        if items[1] in ('afix'[:len1],'afree'[:len1],'acenter'[:len1],
                        'list'[:len1],'run'[:len1],'asa'[:len1],'return'[:len1]):
            if len(items) > 2:
                for i in range(2,len(items)):
                    mac_cmd = mac_cmd + items[i] + ' '
                [state,msg,color] = command_parser(mac_cmd.split(),rod_main)
                if not state:
                    return state,msg,color
        elif items[1] in ('fix'[:len1],'center'[:len1],'free'[:len1]):
            if len(items) > 3:
                for i in range(3,len(items)):
                    mac_cmd = mac_cmd + items[i] + ' '
                [state,msg,color] = command_parser(mac_cmd.split(),rod_main)
                if not state:
                    return state,msg,color # newly edited date: 21/08/2017 
            try:
                int(items[2])   # serial number limite
            except:
                return 0, 'An integer needed','red'
        elif items[1] in ('value'[:len1],'lower'[:len1],'upper'[:len1]): 
            if len(items) > 4:
                for i in range(4,len(items)):
                    mac_cmd = mac_cmd + items[i] + ' '
                [state,msg,color] = command_parser(mac_cmd.split(),rod_main)
                if not state:
                    return state,msg,color
            try:
                int(items[2]) # more constraints need to add here, the serial number limite
                float(items[3])
            except:
                return 0,'Wrong type input(s)','red'
        elif items[1] == 'control'[:len1]:
            len2 = len(items[2])
            wrong_command = True
            for set_controlpars_menu_item in menu['set_controlpars_menu']:  # command guard
                if items[2] == set_controlpars_menu_item[0][:len2]:
                    wrong_command = False
                    break
            if wrong_command:
                return 0, 'Unknown command: {0}'.format(items[2]),'red'
            if items[2] in ('covariance'[:len2],'chisqr'[:len2]) and len(items) != 3:
                return 0,'3 commands needed: {0} {1} {2} {3}'.format(items[0],items[1],items[2],items[3]),'red'
            elif items[2] in ('itermax'[:len2],'conv'[:len2]): 
                if len(items) != 4:
                    return 0,'4 commands needed', 'red'
                elif items[2] == 'itermax'[:len2]:
                    try:
                        int(items[3])
                    except:
                        return 0,'A integer needed','red'
                elif items[2] == 'conv'[:len2]:
                    try:
                        float(items[3])
                    except:
                        return 0,'Pease check command','red'
            rod_main.CommandEntry.set_text('fit control ')
        wrong_command = True
        for fit_menu_item in menu['fit_menu']:
            if items[1] == fit_menu_item[0][:len1]:
                wrong_command = False
                break
        if not wrong_command:
            return 1, 'fit', 'green'
        else:
            return 0, 'Unknown command:{0}'.format(items[1]),'red'
        
    elif items[0] == "fitplus"[:len0]: # will be done in the future (probably)
        return 1, 'fitplus', 'green'
        
    elif items[0] == "list"[:len0]:
        len1 = len(items[1])
        if items[1].lower() in ('pararameters'[0:len1],'surface'[0:len1],'smodel'[:len1],'mmodel'[:len1],'zdensity'[:len1],
                                'data'[0:len1],'fit'[0:len1],'bulk'[0:len1],'molecules'[:len1],'symmetry'[:len1],
                                'sum'[:len1],'all'[:len1],'bmodel'[0:len1],'compare'[:len1],'fatomic'[:len1]):
            if len(items) != 3:
                return 0,'Please check command','red'
            return 1,'listing','green' 
        elif items[1].lower() == 'rodcompare'[:len1]:
            if len(items) != 5:
                return 0,'Please check command','red'
            else:
                try:
                    int(items[3])
                    int(items[4])
                except:
                    return 0,'Integers needed', 'red'
        elif items[1].lower() == 'Bonds'[:len1]:
            if len(items) != 4:
                return 0,'Please check command','red'
            else:
                try:
                    int(items[3])
                except:
                    return 0,'An integer needed', 'red'
        wrong_command = True
        for list_menu_item in menu['llist_menu']:
            if items[1] == list_menu_item[0][:len1]:
                wrong_command = False
                break
        if not wrong_command:
            return 1,'listing other stuff','orange' # need to be modified later
        else:
            return 0, 'Unknown command:{0}'.format(items[1]),'red'
        
    elif items[0] == "calculate"[:len0]:
        if rod_main.Get_Global_Variable('NSURF') + rod_main.Get_Global_Variable('NBULK') < 1:
            return 0,'No model read in','red'
        if len(items) <= 2 :
            if len(items) == 2 and items[1] == 'data'[:len(items[1])]:
                return 1,'Calc all data','green'
            return 0,'No enough commands','red'
        len1 = len(items[1])
        wrong_command = True
        for calc_item in menu['calc_menu']:
            if items[1] == calc_item[0][:len1]:
                wrong_command = False
                break
        if wrong_command:
            return 0,'Unknown commands:\'{0}\''.format(items[1]),'red' 
        if items[1] == 'rod'[:len1]:
            if len(items) < 4:
                return 0,'No enough commands','red'
            elif len(items) > 4:
                return 0,'Too many commands','red'
            else:
                try:
                    float(items[2])
                    float(items[3])
                except ValueError:
                    return 0,'Wrong type inputs: {0} or {1}'.format(items[2],items[3]),'red'
        elif items[1] == 'range'[:len1]:
            if len(items) < 9:
                return 0,'No enough commands','red'
            elif len(items) > 9:
                return 0,'Too many commands','red'
            else:
                for i in range(7):
                    try:
                        exec('float({0})'.format(items[i+2]))
                    except ValueError:
                        return 0,'Wrong type input: {0}'.format(items[i+2]),'red'
                    if float(items[4]) < 0.1 or float(items[7]) < 0.1:
                        return 0,'Too small step size','red'
        elif items[1] == 'qrange'[:len1]:
            if len(items) < 6:
                return 0,'No enough commands','red'
            elif len(items) > 6:
                return 0,'Too many commands','red'
            else:
                for i in range(4):
                    try:
                        exec('float({0})'.format(items[i+2]))
                    except ValueError:
                        return 0,'Wrong type input: {0}'.format(items[i+2]),'red'
                    if float(items[3]) < 0.1 or float(items[4]) < 0.1:
                        return 0,'Too small step size','red'
        elif items[1] == 'data'[:len1]:
            if len(items) > 3:
                return 0,'Too many commands','red'
            elif rod_main.Get_Global_Variable('NDAT') < 1:
                return 0,'No experimental data read in','red'
            elif rod_main.Get_Global_Variable('MAXTHEO') < rod_main.Get_Global_Variable('NDAT'):
                return 0,'More data than theory points','red'
        elif items[1] == 'distance'[:len1]:
            if len(items) < 4:
                return 0,'No enough commands','red'
            elif len(items) > 4:
                return 0,'Too many commands','red'
            for i in range(2):
                try:
                    exec('int({0})'.format(items[i+2]))
                except ValueError:
                    return 0,'Wrong type input: {0}'.format(items[i+2]),'red'
            if rod_main.Get_Global_Variable('NSURF') < 2:
                return 0,'Too few atoms in surface unit cell','red'
            elif int(items[2]) > rod_main.Get_Global_Variable('NSURF') or int(items[3]) > rod_main.Get_Global_Variable('NSURF'):
                return 0,'Only {0} atoms in surface unit cell'.format(rod_main.Get_Global_Variable('NSURF')),'red'
        elif items[1] == 'angle'[:len1]:
            if len(items) < 5:
                return 0,'No enough commands','red'
            elif len(items) > 5:
                return 0,'Too many commands','red'
            for i in range(3):
                try:
                    exec('int({0})'.format(items[i+2]))
                except ValueError:
                    return 0,'Wrong type input: {0}'.format(items[i+2]),'red'
            nsurf = rod_main.Get_Global_Variable('NSURF')
            if nsurf < 3:
                return 0,'Too few atoms in surface unit cell','red'
            elif int(items[2]) > nsurf or int(items[3]) > nsurf or int(items[4]) > nsurf:
                return 0,'Only {0} atoms in surface unit cell'.format(nsurf),'red'
        elif items[1] == 'roughness'[:len1]:
            if len(items) > 3:
                return 0,'Too many commands','red'  # more details might need to be added here
                
        return 1, 'calculating','green'
        
    elif items[0] == "set"[:len0]:
        if len(items) <= 2:
            return 0,'No enough commands','red'
        len1 = len(items[1])
        len2 = len(items[2])
        wrong_command = True
        for set_menu_item in menu['set_menu']:
            if items[1] == set_menu_item[0][:len1]:
                wrong_command = False
                break
        if wrong_command:
            return 0, 'Unkown command:{0}'.format(items[1]),'red'
        if items[1] == 'symmetry'[:len1]:
            for set_symmetry_menu_item in menu['set_symmetry_menu']:
                if items[2] == set_symmetry_menu_item[0][:len2]:
                    wrong_command = False
                    break
            if wrong_command:
                return 0, 'Unknown command:{0}'.format(items[2]),'red'
        elif items[1] == 'calculate'[:len1]:
            if items[2] == 'list'[:len2]:
                if len(items) == 3:
                    return 1,'listing set_calc','green'
                else:
                    return 0,'Unexpected command','red'
            elif len(items) < 4:
                return 0,'No enough commands','red'
            wrong_command = True
            for set_calculate_menu_item in menu['set_calculate_menu']:
                if items[2] == set_calculate_menu_item[0][:len2]:
                    wrong_command = False
                    break
            if wrong_command:
                return 0, 'Unknown command:{0}'.format(items[2]),'red'
            len3 = len(items[3])
            if items[2] in ('lstart'[0:len2],'lend'[0:len2],'beta'[0:len2],
                        'attenuation'[0:len2],'lbragg'[0:len2],'nlayers'[0:len2],
                        'sfraction'[0:len2],'s2fraction'[0:len2],'nsurf2'[0:len2]):  # float
                try:
                    float(items[3])
                except ValueError:
                    return 0,'Wrong type input: {0}'.format(items[3]),'red'
            elif items[2] in ('npoints'[0:len2],'energy'[0:len2],'subscale'[0:len2],): # int
                try:
                    int(items[3])
                except ValueError:
                    return 0,'Wrong type input: {0}'.format(items[3]),'red'
            elif items[2] in ('structure'[0:len2],'fractional'[0:len2],):
                if items[3].lower() not in ('yes'[:len3],'no'[:len3]):
                    return 0,'Wrong input: {0}'.format(items[3]),'red'
            elif items[3] == 'roughness'[0:len2]:
                if items[3].lower() not in ('approx'[:len3],'beta'[:len3],'poisson'[:len3],'gaussian'[:len3],
                                            'linear'[:len3],'cosine'[:len3],'twolevel'[:len3]):
                    return 0,'Wrong input: {0}'.format(items[3]),'red'
            elif items[3] == 'bweight'[:len2]:
                if len(items) < 5:
                    return 0,'No enough commands','red'
                elif len(items) >= 5:
                    try:
                        float(items[3])
                        float(items[4])
                    except ValueError:
                        return 0,'Wrong type input: {0} or {1}'.format(items[3],items[4]),'red'
                    finally:
                        if len(items) > 5:
                            [state,msg,color] = command_parser(['set','calc'] + items[5:],rod_main)
                            if not state:
                                return state,msg,color
                        
        #constraints of set_par (more type constraints need to be added in the future in neccesary)
        elif items[1] == 'parameters'[0:len1]:
            if items[2] == 'list'[:len2]:
                if len(items) == 3:
                    return 1,'listing set_calc','green'
                else:
                    return 0,'Unexpected command','red'
            if len(items) < 7:
                return 0,'No enough commands','red'
            wrong_command = True
            for set_parameters_menu_item in menu['set_parameters_menu']:
                if items[2] == set_parameters_menu_item[0][:len2]:
                    wrong_command = False
                    break
            if wrong_command:
                return 0, 'Unknown command:{0}'.format(items[2]),'red'
            #if rod_main.valid_flag['Fit'] != 1:
            #    rod_main.CommandEntry.set_text('')
             #   return 0,'Par file read failed. Read a valid fit file first', 'red'
            elif items[2] in ('scale'[0:len2],'beta'[0:len2],'surffrac'[0:len2]):
                len6 = len(items[6])
                for i in range(3):
                    try:
                        exec('float({0})'.format(items[i+3]))
                    except ValueError:
                        return 0,'Wrong type input: {0} command'.format(items[i+3]),'red'
                if items[6].lower() not in ('yes'[:len6],'no'[:len6]):
                    return 0,'Wrong input: 7th command','red'
                if len(items) > 7:
                    [state,msg,color] = command_parser(['set','par']+ items[7:],rod_main)
                    if not state:
                        return state,msg,color
            elif items[2] in ('displace'[0:len2],'b1'[0:len2],'b2'[0:len2],
                            'occupancy'[0:len2],'subscale'[0:len2]) and len(items) >= 8:
                len7 = len(items[7])
                tmp_typetuple = ('int','float','float','float')
                for i in range(0,4):
                    try:
                        exec('{0}({1})'.format(tmp_typetuple[i],items[i+3]))
                    except ValueError:
                        return 0,'Wrong type input: {0} command'.format(items[i+3]),'red'
                if items[7].lower() not in ('yes'[:len7],'no'[:len7]):
                    return 0,'Wrong input: 8th command','red'
                if len(items) > 8:
                    [state,msg,color] = command_parser(['set','par']+ items[8:],rod_main)
                    if not state:
                        return state,msg,color
        #constraints of set_domain
        elif items[1] == 'domain'[0:len1]:
            if items[2] == 'list'[:len2]:
                if len(items) == 3:
                    return 1,'listing set_calc','green'
                else:
                    return 0,'Unexpected command','red'
            if len(items) < 4:
                return 0,'No enough commands','red'
            wrong_command = True
            for set_domain_menu_item in menu['set_domain_menu']:
                if items[2] == set_domain_menu_item[0][:len2]:
                    wrong_command = False
                    break
            if wrong_command:
                return 0, 'Unknown command:{0}'.format(items[2]),'red'
            if items[2] in ('equal'[0:len2],'coherent'[0:len2],'fitoccup'[0:len2],
                            'fractional'[0:len2],):
                if len(items) < 4:
                    return 0,'No enough commands','red'
                elif len(items) >= 4:
                    if items[3].lower() not in ('yes'[:len(items[3])],'no'[:len(items[3])]):
                        return 0,'Wrong command: \'{0}\''.format(items[3]),'red'
                    if len(items) > 4:
                        [state,msg,color] = command_parser(['set','dom']+ items[4:],rod_main)
                        if not state:
                            return state,msg,color
                            
            elif items[2] == 'ndomains'[0:len2]:
                if len(items) >= 4:
                    try:
                        int(items[3])
                    except ValueError:
                        return 0,'Wrong input: \'{0}\''.format(items[3]),'red'
                    finally:
                        if len(items) > 4:
                            if items[4] == 'matrix'[:len(items[4])]:
                                pass_value = int(items[3])
                            [state,msg,color] = command_parser(['set','dom'] + items[4:],rod_main,pass_value)
                            if not state:
                                return state,msg,color
                    
            elif items[2] == 'occupancy'[0:len2]:
                if rod_main.Get_Global_Variable('DOMEQUAL') == 1:
                    return 0,'All domains are set equal','red'
                elif len(items) < 5:
                    return 0,'No enough commands','red'
                elif len(items) >= 5:
                    try:
                        int(items[3])
                        float(items[4])
                    except ValueError:
                        return 0,'Wrong type inputs: \'{0}\' or \'{1}\''.format(items[3],items[4]),'red'
                    finally:
                        if len(items) > 5:
                            [state,msg,color] = command_parser(['set','dom'] + items[5:],rod_main)
                            if not state:
                                return state,msg,color
            
            elif items[2] == 'matrix'[0:len2]:
                tmp_typetuple = ('int','float','float','float','float')
                for i in range(5):
                    try:
                        exec('{0}({1})'.format(tmp_typetuple[i],items[i+3]))
                    except ValueError:
                        return 0,'Wrong type inpyt: {0}'.format(items[i+3]),'red'
                if pass_value != None:
                    if int(items[3]) > pass_value:
                        return 0, 'Domain serial number should be smaller than {0}'.format(pass_value+1),'red'
                elif int(items[3]) > rod_main.Get_Global_Variable('NDOMAIN'):
                    return 0, 'Domain serial number should be smaller than {0}'.format(rod_main.Get_Global_Variable('NDOMAIN')+1),'red'
                if len(items) > 8:
                    [state,msg,color] = command_parser(['set','dom'] + items[5:],rod_main)
                    if not state:
                        return state,msg,color
        return 1,'Setting','green' # need to be modified later
    return 0,'all in all (you missed this command)', 'red' # if you reach this step, means there is something probably wrong and this line is for debugging

def display_file(rod_main,extension):
        
    for child in rod_main.CommandHelper_VBox.get_children():
        rod_main.CommandHelper_VBox.remove(child)
    filelist = os.listdir(".")
    for file in filelist:
        if file.endswith(extension):
            button = gtk.Button(file)
            button.get_child().set_alignment(0, 0.5)
            button.connect("clicked", menu_button_clicked,rod_main)
            rod_main.CommandHelper_VBox.pack_start(button, False, False, 0)
    rod_main.CommandHelper_VBox.show_all()
    
def check_file(rod_main,extension,file_name):
    
    filelist = os.listdir(".")
    for file in filelist:
        if file.endswith(extension) and file_name == file.split('.')[0]:
            return 1
    return 0
            
def menu_button_clicked(widget,rod_main):
        
    text = rod_main.CommandEntry.get_text()
    if text == "":
        text = widget.get_label()
    elif text[-1] == " ":
        text = text + widget.get_label()
    else:
        text = " ".join(text.split()[:-1]+[widget.get_label()])
    if '.' in text:
        text = text.split('.')[0]
    rod_main.CommandEntry.set_text(text+' ')
  
        
def display_menu(rod_main, menu):
    
    for child in rod_main.CommandHelper_VBox.get_children():
        rod_main.CommandHelper_VBox.remove(child)
    for item in menu:
        hbox = gtk.HBox(homogeneous = False, spacing = 0)
        label = gtk.Label(item[1])
        label.set_alignment(0, 0.5)
        button = gtk.Button(item[0])
        button.get_child().set_alignment(0, 0.5)
        button.set_size_request(100, 25)
        button.connect("clicked", menu_button_clicked,rod_main)
        hbox.pack_start(button, False, False, 0)
        hbox.pack_start(label, True, True, 10)
        rod_main.CommandHelper_VBox.pack_start(hbox, False, False, 0)
    rod_main.CommandHelper_VBox.show_all()
    
    
def display_tips(rod_main, tips):
    
    for child in rod_main.CommandHelper_VBox.get_children():
        rod_main.CommandHelper_VBox.remove(child)
    
    for tip in tips:
        hbox = gtk.HBox(homogeneous = False, spacing = 0)
        label = gtk.Label(tip[1])
        label.set_alignment(0, 0.5)
        button = gtk.Button(tip[0])
        button.get_child().set_alignment(0, 0.5)
        button_length = 10*len(tip[0])+5
        button.set_size_request(button_length, 25)
        button.connect("clicked", menu_button_clicked)
        hbox.pack_start(button, False, False, 0)
        hbox.pack_start(label, True, True, 10)
        rod_main.CommandHelper_VBox.pack_start(hbox, False, False, 0)
    rod_main.CommandHelper_VBox.show_all()
        
def command_helper(widget,rod_main):
    
    cmd_text = widget.get_text()
    cmd_items = cmd_text.split()
    if not len(cmd_items):
        if rod_main.cmd_flag != "main":
            display_menu(rod_main,menu['main_menu'])
            rod_main.cmd_flag = "main"
        return 
    
    match = False
    for item in menu['main_menu']:
        if item[0][:len(cmd_items[0])] == cmd_items[0]:
            match = True
            break
    if not match or (len(cmd_items)==1 and cmd_text[-1] != " "):
        if rod_main.cmd_flag != "main":
            display_menu(rod_main,menu['main_menu'])
            rod_main.cmd_flag = "main"
        return 
    else:
        if item[0] == "read":
            if (len(cmd_items)==1 and widget.get_text()[-1] == " ") or\
               (len(cmd_items)==2 and widget.get_text()[-1] != " "):
                if rod_main.cmd_flag != "read":
                    rod_main.cmd_flag = "read"
                    display_menu(rod_main,menu['read_menu'])
                return
            else:
                match = False
                for item in menu['read_menu']:
                    if item[0][:len(cmd_items[1])] == cmd_items[1]:
                        match = True
                        break
                if not match:
                    if rod_main.cmd_flag != "read":
                        display_menu(rod_main,menu['read_menu'])
                        rod_main.cmd_flag = "read"
                    return 
                else:
                    if item[0] == "data" and rod_main.cmd_flag != "data":
                        display_file(rod_main,".dat")
                        rod_main.cmd_flag = "data"
                    elif item[0] == "surface" and rod_main.cmd_flag != "surface":
                        display_file(rod_main,".sur")
                        rod_main.cmd_flag = "surface"
                    elif item[0] == "bulk" and rod_main.cmd_flag != "bulk":
                        display_file(rod_main,".bul")
                        rod_main.cmd_flag = "bulk"
                    elif item[0] == "fit" and rod_main.cmd_flag != "fit":
                        display_file(rod_main,".fit")
                        rod_main.cmd_flag = "fit"
                    elif item[0] == "molecule" and rod_main.cmd_flag != "molecule":
                        display_file(rod_main,".mol")
                        rod_main.cmd_flag = "molecule"
                    elif item[0] == "parameters" and rod_main.cmd_flag != "parameters":
                        display_file(rod_main,".par")
                        rod_main.cmd_flag = "parameters"
                    elif item[0] == "fatomic" and rod_main.cmd_flag != "fatomic":
                        display_file(rod_main,".fat")
                        rod_main.cmd_flag = "fatomic"
                    elif item[0] == "macro" and rod_main.cmd_flag != "macro":
                        display_file(rod_main,".mac")
                        rod_main.cmd_flag = "macro"
        elif item[0] == "list": 
            if (len(cmd_items)==1 and widget.get_text()[-1] == " ") or\
               (len(cmd_items)==2 and widget.get_text()[-1] != " "):
                if rod_main.cmd_flag != "list":
                    rod_main.cmd_flag = "list"
                    display_menu(rod_main,menu['llist_menu'])
                return          
            else:
                if rod_main.cmd_flag != "list2":
                    rod_main.cmd_flag = "list2"
                    tips = [["t", "print to terminal"],\
                           ["filename comment", "print to file, first line comment"]]
                    display_tips(rod_main,tips)
                return    
        elif item[0] == "fit":
            if (len(cmd_items)==1 and widget.get_text()[-1] == " ") or\
               (len(cmd_items)==2 and widget.get_text()[-1] != " "):
                if rod_main.cmd_flag != "fit":
                    rod_main.cmd_flag = "fit"
                    display_menu(rod_main,menu['fit_menu'])
                return   
            else:
                match = False
                cmd_len = len(cmd_items) if cmd_text[-1] != ' ' else len(cmd_items)+1
                #print cmd_len, "cmd_len after given value"
                for item in menu['fit_menu']:
                    if item[0][:len(cmd_items[1])] == cmd_items[1]:
                        match = True
                        break
                if not match:
                    if rod_main.cmd_flag != "fit":
                        display_menu(rod_main,menu['fit_menu'])
                        rod_main.cmd_flag = "fit"
                    return 
                else:
                    if item[0] == "value":
                        if cmd_len == 3 :
                            tips = [["1","Parameter value to be changed"]]
                        elif cmd_len == 4 :
                            tips = [["0","new value"]] # need to be modified in the  near future
                        else:
                            if cmd_len != 3:
                                tips = [["error","Too many parameters"]]
                    elif item[0] == "lower":
                        if cmd_len == 3 :
                            tips = [["1","Minimum of parameter"]]
                        elif cmd_len == 4 :
                            tips = [["0","new value"]] # need to be modified in the  near future
                        else:
                            if cmd_len != 3:
                                tips = [["error","Too many parameters"]]
                    elif item[0] == "upper":
                        if cmd_len == 3 :
                            tips = [["1","Maximum of parameter"]]
                        elif cmd_len == 4 :
                            tips = [["0","new value"]] # need to be modified in the  near future
                        else:
                            if cmd_len != 3:
                                tips = [["error","Too many parameters"]]
                    if item[0] == "fix":
                        tips = [["1","Parameter to be fixed"]]
                    elif item[0] == "free":
                        tips = [["1","Parameter to be free"]]
                    elif item[0] == "center":
                        tips = [["1","Center parameter"]]
                    elif item[0] == "afix":
                        tips = [["all fixd","All parameters fixed"]]
                    elif item[0] == "afree":
                        tips = [["all free","All parameter free"]]
                    elif item[0] == "acenter":
                        tips = [["all center","Center all free parameters"]]
                    elif item[0] == "control":#do it later 15/06/2017
                        display_menu(rod_main,menu["set_controlpars_menu"])
                        if (len(cmd_items)== 3 and widget.get_text()[-1] == " ") or\
                            (len(cmd_items)== 4 and widget.get_text()[-1] != " "):
                            for sub_item in menu["set_controlpars_menu"]:
                                if sub_item[0][:len(cmd_items[2])] == cmd_items[2]:
                                    if sub_item[0] == 'covariance':
                                        tips = [['covariance','Use covariance matrix for error estimate']]
                                    elif sub_item[0] == 'chisqr':
                                        tips = [['chisqr','Use chi^2 for error estimate']]
                                    elif sub_item[0] == 'itermax':
                                        tips = [['itermax','Set the maximum number of iterations(L/M fitting only)']]
                                    elif sub_item[0] == 'conv':
                                        tips = [['Conv','chi^2 convergence criterion']]
                                    elif sub_item[0] == 'list':
                                        tips = [['List','List control parameters']]
                                    display_tips(rod_main,tips)
                    elif item[0] == "list":
                        tips = [["list","List parameter values"]]
                    elif item[0] == "run":
                        tips = [["run","Start fit (Levenberg Marquardt)"]]
                    elif item[0] == "asa":
                        tips = [["asa","Start fit (adaptive simulated annealing)"]]
                    elif item[0] == "return":
                        tips = [["return","Return"]]
                    rod_main.cmd_flag = "fit_tips"
                    if item[0] != 'control':
                        display_tips(rod_main,tips)
        elif item[0] == "calculate": 
            if (len(cmd_items)==1 and widget.get_text()[-1] == " ") or\
               (len(cmd_items)==2 and widget.get_text()[-1] != " "):
                if rod_main.cmd_flag != "calculate":
                    rod_main.cmd_flag = "calculate"
                    display_menu(rod_main,menu['calc_menu'])
                return    
            else:
                match = False
                cmd_len = len(cmd_items) if cmd_text[-1] != ' ' else len(cmd_items)+1
                for item in menu['calc_menu']:
                    if item[0][:len(cmd_items[1])] == cmd_items[1]:
                        match = True
                        break
                if not match:
                    if rod_main.cmd_flag != "calculate":
                        display_menu(rod_main,menu['calc_menu'])
                        rod_main.cmd_flag = "calculate"
                    return 
                else:
                    if item[0] == "rod":
                        if cmd_len == 3:
                            tips = [["1.0","Diffraction index h"]]
                        elif cmd_len == 4:
                            tips = [["1.0","Diffraction index k"]]
                        else: 
                            tips = [["error","Too many parameters"]]
                    elif item[0] == "range":
                        if cmd_len == 3:
                            tips = [["0.0","Start value of diffraction index h"]]
                        elif cmd_len == 4:
                            tips = [["0.0","End value of diffraction index h"]]
                        elif cmd_len == 5:
                            tips = [["0.0","Step size of diffraction index h"]]
                        elif cmd_len == 6:
                            tips = [["0.0","Start value of diffraction index k"]]
                        elif cmd_len == 7:
                            tips = [["0.0","End value of diffraction index k"]]
                        elif cmd_len == 8:
                            tips = [["0.0","Step size of diffraction index k"]]
                        elif cmd_len == 9:
                            tips = [["0.0","Diffraction index l"]]
                        else:
                            #print cmd_len
                            tips = [["error","Too many parameters"]]
                    elif item[0] == "qrange":
                        if cmd_len == 3:
                            tips = [["0.00","Maximum value of q(in terms of h)"]]
                        elif cmd_len == 4:
                            tips = [["3.0","Step size of diffraction index h"]]
                        elif cmd_len == 5:
                            tips = [["1.0","Step size of diffraction index k"]]
                        elif cmd_len == 6:
                            tips = [["1.0","Difffraction index l"]]
                        else:
                            tips = [["error","Too many parameters"]]
                    elif item[0] == "data":
                        tips = [["data","Calculate f´s for all data points"]]
                    elif item[0] == "distance":
                        if cmd_len == 3:
                            tips = [["1","First atom"]]
                        elif cmd_len == 4:
                            tips = [["2","Second atom"]]
                    elif item[0] == "angle":
                        if cmd_len == 3:
                            tips = [["1","First atom"]]
                        elif cmd_len == 4:
                            tips = [["2","Second(center) atom"]]
                        elif cmd_len == 5:
                            tips = [["3","Third atom"]]
                        else:
                            tips = [["error","Too many parameters"]]
                    elif item[0] == "roughness":
                        tips = [["roughness","calculate the roughness in atomic layers"]]
                    elif item[0] == "return":
                        tips = [["return","Return"]]
                    rod_main.cmd_flag = "cal_tips"
                    display_tips(rod_main,tips)
        elif item[0] == "set":
            if (len(cmd_items)==1 and widget.get_text()[-1] == " ") or\
               (len(cmd_items)==2 and widget.get_text()[-1] != " "):
                if rod_main.cmd_flag != "set":
                    rod_main.cmd_flag = "set"
                    display_menu(rod_main,menu['set_menu'])
                return
            else:
                match = False
                for item in menu['set_menu']:
                    if item[0][:len(cmd_items[1])] == cmd_items[1]:
                        match = True
                        break
                if not match:
                    if rod_main.cmd_flag != "set":
                        display_menu(rod_main,menu['set_menu'])
                        rod_main.cmd_flag = "set"
                    return 
                else:                   
                    if item[0] == "calculate" :
                        if (len(cmd_items)== 2 and widget.get_text()[-1] == " ") or\
                           (len(cmd_items)== 3 and widget.get_text()[-1] != " "):
                               if rod_main.cmd_flag != "set_calculate" :    
                                   display_menu(rod_main,menu['set_calculate_menu'])
                                   rod_main.cmd_flag = "set_calculate"
                        else:
                            match = False
                            cmd_len = len(cmd_items) if cmd_text[-1] != ' ' else len(cmd_items)+1
                            for item in menu['set_calculate_menu']:
                                if item[0][:len(cmd_items[2])] == cmd_items[2]:
                                    match = True
                                    break
                            if not match or cmd_len == 3:
                                if rod_main.cmd_flag != "set_calculate":
                                    display_menu(rod_main,menu['set_calculate_menu'])
                                    rod_main.cmd_flag = "set_calculate"
                            else:
                                if item[0] == "structure":
                                    tips = [["YES","Use structure factors (else intensities)"]]
                                elif item[0] == "lstart":
                                    tips = [["-3.9","Start value of l"]]
                                elif item[0] == "lend":
                                    tips = [["3.9","End value of l"]]
                                elif item[0] == "npoints":
                                    tips = [["200","Number of points on rod"]]
                                elif item[0] == "atten":
                                    tips = [["0.001","Attenuation factor of X-ray beam"]]
                                elif item[0] == "beta":
                                    tips = [["0.000","Roughness parameter beta"]]
                                elif item[0] == "lbragg":
                                    tips = [["0.00","l-value of nearest Bragg peak"]]
                                elif item[0] == "fractional":
                                    tips = [["NO","Fractional rod"]]
                                elif item[0] == "energy":
                                    tips = [["0","Serial number of X-ray energy"]]
                                elif item[0] == "nlayers":
                                    tips = [["1","Number of layers in bulk unit cell"]]
                                elif item[0] == "scale":
                                    tips = [["0.25","Scale factor of theory"]]
                                elif item[0] == "subscale":
                                    tips = [["0 for off","Serial number active subscale parameter"]]
                                elif item[0] == "sfraction":
                                    tips = [["1.00","Fraction of reconstructed surface"]]
                                elif item[0] == "s2fraction":
                                    tips = [["0.00","Fraction of reconstructed surface with 2nd unit cell"]]
                                elif item[0] == "nsurf2":
                                    tips = [["0","Number atoms in structure belonging to 2nd unit cell"]]
                                elif item[0] == "roughness":
                                    if rod_main.cmd_flag != "set_calculate_roughness" :    
                                        display_menu(rod_main,menu['set_calculate_roughness_menu'])
                                        rod_main.cmd_flag = "set_calculate_roughness"
                                        #some menu need to be write here
                                elif item[0] == "bweight":
                                    if cmd_len == 4:
                                        tips = [["0.00","Active l-interval around Bragg peak [%4.2f]: "]]
                                    elif cmd_len == 5:
                                        tips = [["0.00","Fractional error at Bragg peak [%4.2f]: "]]
                                    else:
                                        tips = [["error","Too many parameters"]]
                                        #no bulk read in
                                        #this bweight need to be corrected in the near future
                                        # two returns need to be added after command
                                elif item[0] == "list":
                                    tips = [["list","list parameters"]]
                                elif item[0] == "help":
                                    if rod_main.cmd_flag != "set_calculate":
                                        display_menu(rod_main,menu['set_calculate_menu'])
                                        rod_main.cmd_flag = "set_calculate"
                                rod_main.cmd_flag = "set_calculate_tips"
                                display_tips(rod_main,tips)
                    elif item[0] == "parameters" :
                        if (len(cmd_items)== 2 and widget.get_text()[-1] == " ") or\
                           (len(cmd_items)== 3 and widget.get_text()[-1] != " "):
                               if rod_main.cmd_flag != "set_parameters" :    
                                   display_menu(rod_main,menu['set_parameters_menu'])
                                   rod_main.cmd_flag = "set_parameters"
                        else:
                            match = False
                            cmd_len = len(cmd_items) if cmd_text[-1] != ' ' else len(cmd_items)+1
                            for item in menu['set_parameters_menu']:
                                if item[0][:len(cmd_items[2])] == cmd_items[2]:
                                    match = True
                                    break
                            if not match or cmd_len == 3:
                                if rod_main.cmd_flag != "set_parameters":
                                    display_menu(rod_main,menu['set_parameters_menu'])
                                    rod_main.cmd_flag = "set_parameters"
                                return 
                            else:
                                if item[0] == "scale" or item[0] == "beta" or item[0] == "surffrac":
                                    if cmd_len == 4:
                                        tips = [["0", "{0} factor of theory".format(item[0])]]
                                    elif cmd_len == 5:
                                        tips = [["0", "Lower limit of {0} factor".format(item[0])]]
                                    elif cmd_len == 6:
                                        tips = [["0", "Upper limit of {0} factor".format(item[0])]]
                                    elif cmd_len == 7:
                                        tips = [["No", "Fit parameter?"]]
                                    else:
                                        tips = [["", "Too many parameters"]]
                                else:
                                    if cmd_len == 4:
                                        tips = [["0", "Serial number of {0} parameter".format(item[0])]]
                                    elif cmd_len == 5:
                                        tips = [["0", "Value of {0} {1}".format(item[0], cmd_items[3])]]
                                    elif cmd_len == 6:
                                        tips = [["0", "Lower limit of {0} {1}".format(item[0], cmd_items[3])]]
                                    elif cmd_len == 7:
                                        tips = [["0", "Upper limit of {0} {1}".format(item[0], cmd_items[3])]]
                                    elif cmd_len == 8:
                                        tips = [["No", "Fit {0} {1}?".format(item[0], cmd_items[3])]]
                                    else:
                                        tips = [["", "Too many parameters"]]
                                rod_main.cmd_flag = "set_parameters_tips"
                                display_tips(rod_main,tips)
                    #set_domain here
                    elif item[0] == "domain":
                        if (len(cmd_items)== 2 and widget.get_text()[-1] == " ") or\
                           (len(cmd_items)== 3 and widget.get_text()[-1] != " "):
                               if rod_main.cmd_flag != "set_domain" :    
                                   display_menu(rod_main,menu['set_domain_menu'])
                                   rod_main.cmd_flag = "set_domain"
                        else:
                            match = False
                            cmd_len = len(cmd_items) if cmd_text[-1] != ' ' else len(cmd_items)+1
                            for item in menu['set_domain_menu']:
                                if item[0][:len(cmd_items[2])] == cmd_items[2]:
                                    match = True
                                    break
                            if not match or cmd_len == 3:
                                if rod_main.cmd_flag != "set_domain":
                                    display_menu(rod_main,menu['set_domain_menu'])
                                    rod_main.cmd_flag = "set_domain"
                                return 
                            else:
                                if item[0] == "ndomains":
                                    tips = [["1","Total number of rotational surface domains"]]
                                elif item[0] == "matrix":
                                    if cmd_len == 4:
                                        tips = [["1","Domain to be modified"]]
                                    elif cmd_len == 5:
                                        tips = [["1.000","Matrix element 11"]]
                                    elif cmd_len == 6:
                                        tips = [["0.000","Matrix element 12"]]
                                    elif cmd_len == 7:
                                        tips = [["0.000","Matrix element 21"]]
                                    elif cmd_len == 8:
                                        tips = [["1.000","Matrix element 22"]]
                                    else:
                                        tips = [["", "Too many parameters"]]
                                elif item[0] == "fractional":
                                    tips = [["YES","Include fractional order indices"]]
                                elif item[0] == "equal":
                                    tips = [["YES","All domains equal occupancy"]]
                                elif item[0] == "occupancy":
                                    if cmd_len == 4:
                                        tips = [["1","Domain to be modified"]]
                                    elif cmd_len == 5:
                                        tips = [["1.000","Occupancy of {} domain".format(cmd_items[3])]]#need to be modified here
                                    else:
                                        tips = [["error","Too many parameters"]]
                                elif item[0] == "coherent":
                                    tips = [["NO","Add domains coherently"]]
                                elif item[0] == "fitoccup":
                                    tips = [["NO","Fit domain occupancy values"]]
                                elif item[0] == "list":
                                    tips = [["list","List settings"]]
                                #if item[0] == "help":
                                    #tips = [["help","List menu"]]
                                elif item[0] == "return":
                                    tips = [["return",""]]
                                rod_main.cmd_flag = "set_domain_tips"
                                display_tips(rod_main,tips)
                    elif item[0] == "plot":
                        if (len(cmd_items)== 2 and widget.get_text()[-1] == " ") or\
                           (len(cmd_items)== 3 and widget.get_text()[-1] != " "):
                               if rod_main.cmd_flag != "set_plot" :    
                                   display_menu(rod_main,menu['set_plot_menu'])
                                   rod_main.cmd_flag = "set_plot"
                        else:
                            match = False
                            cmd_len = len(cmd_items) if cmd_text[-1] != ' ' else len(cmd_items)+1
                            for item in menu['set_plot_menu']:
                                if item[0][:len(cmd_items[2])] == cmd_items[2]:
                                    match = True
                                    break
                            if not match or cmd_len == 3:
                                if rod_main.cmd_flag != "set_plot":
                                    display_menu(rod_main,menu['set_plot_menu'])
                                    rod_main.cmd_flag = "set_plot"
                                return 
                            else:
                                if item[0] == "size":
                                    tips = [["0.050","Size of f-circles(fraction of x-axis)"]]
                                elif item[0] == "threshold":
                                    tips = [["0.000","Plotting threshold of f-circles(fraction of f_max)"]]
                                elif item[0] == "radius":
                                    if cmd_len == 4:
                                        tips = [["element symbol","A valid symbol"]]
                                    elif cmd_len == 5:
                                        tips = [["0.100","Radius of {}".format(cmd_items[3])]]
                                    else:
                                        tips = [["error","Too many parameters"]]
                                elif item[0] == "xmincont":
                                    tips = [["0.00","Lower bound on x range(reduced coordinates)"]]
                                elif item[0] == "xmaxcont":
                                    tips = [["0.00","Upper bound on x range(reduced coordinates)"]]
                                elif item[0] == "nxcontour":
                                    tips = [["0","Number of steps in x range"]]
                                elif item[0] == "ymincont":
                                    tips = [["0.00","Lower bond on y range(reduced coordinates)"]]
                                elif item[0] == "ymaxcont":
                                    tips = [["0.00","Upper bond on y range(reduced coordinates)"]]
                                elif item[0] == "nycontour":
                                    tips = [["0","Number of steps in y range"]]
                                elif item[0] == "minlevel":
                                    tips = [["0.00","Minimum contour level (-/+ fraction of minimunm/maximum)"]]
                                elif item[0] == "maxlevel":
                                    tips = [["0.00","Maximum contour level (-/+ fraction of minimunm/maximum)"]]
                                elif item[0] == "nlevel":
                                    tips = [["0","Number of contours"]]
                                elif item[0] == "list":
                                    tips = [["list","List parameters"]]
                                elif item[0] == "return":
                                    tips = [["return",""]]
                                rod_main.cmd_flag = "set_plot_tips"
                                display_tips(rod_main,tips)
                        
                    elif item[0] == "symmetry":
                        if (len(cmd_items)== 2 and widget.get_text()[-1] == " ") or\
                           (len(cmd_items)== 3 and widget.get_text()[-1] != " "):
                            if rod_main.cmd_flag != "set_symmetry" :    
                                display_menu(rod_main,menu['set_symmetry_menu'])
                                rod_main.cmd_flag = "set_symmetry"
                        elif len(cmd_items) == 3 and widget.get_text()[-1] == " ":
                            if cmd_items[2] ==  'list'[:len(cmd_items[2])]:
                                tips = [["list","List parameters"]]
                                rod_main.cmd_flag = "set_symmetry_tips"
                                display_tips(rod_main,tips)
                        else:
                            return
                    elif item[0] == "fatomic":
                        if (len(cmd_items)== 2 and widget.get_text()[-1] == " ") or\
                           (len(cmd_items)== 3 and widget.get_text()[-1] != " "):
                               if rod_main.cmd_flag != "set_fatomic" :    
                                   display_menu(rod_main,menu['set_fatomic_menu'])
                                   rod_main.cmd_flag = "set_fatomic"
                        else:
                            match = False
                            cmd_len = len(cmd_items) if cmd_text[-1] != ' ' else len(cmd_items)+1
                            for item in menu['set_fatomic_menu']:
                                if item[0][:len(cmd_items[2])] == cmd_items[2]:
                                    match = True
                                    break
                            if not match or cmd_len == 3:
                                if rod_main.cmd_flag != "set_fatomic":
                                    display_menu(rod_main,menu['set_fatomic_menu'])
                                    rod_main.cmd_flag = "set_fatomic"
                                return 
                            else:
                                if item[0] == "fatomic" and cmd_len < 14:
                                    tips_total = [["element symbol","Element to modify"],["","a1"],["","b1"],
                                                  ["","a2"],["","b2"],["","a3"],["","b3"],["","a4"],["","b4"],["","c"]]
                                    tips = [tips_total[cmd_len-4]]
                                elif item[0] == "dispersion" and cmd_len < 7 :
                                    tips_total = [["element symbol","Element to modify"],["0.0000","f1"],["0.0000","f2"]]
                                    tips = [tips_total[cmd_len-4]]
                                elif item[0] == "list":
                                    tips = [["list","List parameters"]]
                                elif item[0] == "return":
                                    tips = [["return","Return"]]
                                rod_main.cmd_flag = "set_fatomic_tips"
                                display_tips(rod_main,tips)
                                    
        elif item[0] == "plot":
            tips = [['None','Plot has been replaced by Python(enter)']]
            display_tips(rod_main,tips)
        elif item[0] == 'energy':
            if (len(cmd_items)==1 and widget.get_text()[-1] == " ") or\
               (len(cmd_items)==2 and widget.get_text()[-1] != " "):
                if rod_main.cmd_flag != "energy":
                    rod_main.cmd_flag = "energy"
                    display_menu(rod_main,menu['energy_menu'])
                return
            else:
                match = False
                for item in menu['energy_menu']:
                    if item[0][:len(cmd_items[1])] == cmd_items[1]:
                        match = True
                        break
                if not match:
                    if rod_main.cmd_flag != "energy":
                        display_menu(rod_main,menu['energy_menu'])
                        rod_main.cmd_flag = "energy"
                    return 
                else:                   
                    if item[0] == "alpha":
                        tips = [['alpha','Bond length deformation parameter alpha: {0:7.4f}'.format(rod_main.Get_Global_Variable('ALPHA_KEAT'))]]
                    elif item[0] == "beta":
                        tips = [['beta','Bond angle deformation parameter beta: {0:7.4f}'.format(rod_main.Get_Global_Variable('BETA_KEAT'))]]
                    display_tips(rod_main,tips)