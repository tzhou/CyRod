# -*- coding: utf-8 -*-
####################################################################################
####    CyRod - Python Wrapper for rod                                          ####
####    Email to nrszhou@gmail.com for any questions or comments                ####
####    Version : 1.0.0 (Feb. 2016)                                             ####
####    Project site: wait....                                                  ####
####################################################################################


import pygtk
import gtk
import os
import numpy
from math import *

elements = [\
"H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg",\
"Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr",\
"Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br",\
"Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd",\
"Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La",\
"Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er",\
"Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir","Pt","Au",\
"Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th",\
"Pa","U","Np","Pu","Am","Cm","Bk","Cf",\
"E1","E2","E3","E4","E5"]

class MyTableWindow:
    
    Table_Clipboard = []
    
    def show_fitresults(self):  # only for Table_Par
    
        for row in self.Table_ListStore: # set background for bad values
            if str(row[7]) == 'True':
                row[11] = False
                row[12] = False
                row[13] = False
            else:
                if str(row[8]) == 'True':
                    row[11] = True if float(row[9]) <= float(row[5]) else False
                    row[12] = True if float(row[9]) >= float(row[6]) else False
                else:
                    row[11] = False
                    row[12] = False
                row[13] = True if fabs(float(row[4])) < fabs(float(row[10])) else False  
    
    
    def Validate_CellValues(self, widget):
        '''validate READ files (bul sur fit par dat)'''
        
        # for the .par file, verify if .fit file is valid, and if not, try to validate it first
        if self.filetype == 'Par':
            if self.rod_main.valid_flag['Fit'] == -1:
                self.rod_main.Update_Status('Please create a valid .fit file first!', 'red')
                return 0
            elif self.rod_main.valid_flag['Fit'] == 0:
                self.rod_main.valid_flag["Fit"] = self.rod_main.Table_Fit.Validate_CellValues(None)
                if self.rod_main.valid_flag['Fit'] == 0:
                    self.rod_main.Update_Status('Please correct the .fit file first!', 'red')
                    return 0
            if self.rod_main.valid_flag['Fit']: # if validated
                # validate table values
                flag_all = True
                for row in self.Table_ListStore:   # row loop
                    flag_row = True
                    if row[2] not in ('scale','surffrac','beta'):
                        row_3 = float(row[3])
                        if row[2].lower() == 'displace':# check the parameter serial number limite for each line
                            if row_3 > self.rod_main.Get_Global_Variable('NDISTOT'):
                                flag_row = False
                        elif row[2].lower() == 'b1':
                            if row_3 > self.rod_main.Get_Global_Variable('NDWTOT'):
                                flag_row = False
                        elif row[2].lower() == 'b2':
                            if row_3 > self.rod_main.Get_Global_Variable('NDWTOT2'):
                                flag_row = False
                        elif row[2].lower() == 'occupancy':
                            if row_3 > self.rod_main.Get_Global_Variable('NOCCTOT'):
                                flag_row = False
                        elif row[2].lower() == 'subscale':
                            if row_3 > self.rod_main.Get_Global_Variable('NSUBSCALETOT'):
                                flag_row = False
                        elif row[2].lower() == 'molecules':
                            if row_3 > self.rod_main.Get_Global_Variable('NMOLECULES'):
                                flag_row = False
                        else:  # parameters should be one of them return False if not founded
                            flag_row = False
                    for i in range(len(row)):       # sub loop in row
                        if self.celltype[i] == 'val':
                            try:
                                value = float(row[i])
                            except TypeError:
                                if row[i] == None:
                                    row[i] = '{0: .5f}'.format(0)
                            except ValueError:
                                flag_row = False
                            else:
                                row[i] = '{0: .5f}'.format(value) # this step is to make it look better
                        row[1] = False if flag_row else True
                        flag_all &= flag_row
            else:
                self.rod_main.Update_Status('Read a valid fit file first','red')
                return 0
        elif self.filetype in ['Bul', 'Sur', 'Fit']:
            # validate lattice parameters for .bul, .sur and .fit
            flag_lat = True
            for i in range(6):
                try:
                    value = eval("float(self.Lattice_lat{0}_Entry.get_text())".format(i))
                except ValueError:
                    flag_lat = False
                    # exec("self.Lattice_lat{0}_Entry.set_text('ERROR')".format(i))
                else:
                    exec("self.Lattice_lat{0}_Entry.set_text('{1:.4f}')".format(i, value))
            if flag_lat:
                self.Lattice_Label.set_markup('<span color= "black"> LAT: </span>')
            else:
                self.Lattice_Label.set_markup('<span color= "red"> LAT: </span>')
            # validate table values
            flag_all = True
            for row in self.Table_ListStore:
                flag_row = True
                for i in range(len(row)):
                    if self.celltype[i] == 'el':
                        if row[i] not in elements:
                            flag_row = False
                        else:
                            pass
                    elif self.celltype[i] == 'val':  # to know its type
                        try:
                            value = float(row[i])
                        except ValueError:
                            flag_row = False
                        else:
                            row[i] = '{0: .5f}'.format(value)
                    elif self.celltype[i] in ['displ', 'occ', 'dw']:
                        try:
                            value = int(row[i])
                        except TypeError:
                            if row[i] == None:
                                row[i] = str(0)
                        except ValueError:
                            flag_row = False
                        else:
                            row[i] = str(value)
                    row[1] = False if flag_row else True
                    flag_all &= flag_row
        elif self.filetype == 'Dat':
            # validate table values
            flag_all = True
            for row in self.Table_ListStore:
                flag_row = True
                for i in range(len(row)-1):
                    if self.celltype[i] == 'HK':
                        try:
                            value = float(row[i])
                        except ValueError:
                            flag_row = False
                        else:
                            if numpy.fabs(value-int(value)) <= 1e-3:
                                row[i] = str(int(value))
                            else:
                                flag_row = False
                    elif self.celltype[i] == 'L' or self.celltype[i] == 'F':
                        try:
                            value = float(row[i])
                        except ValueError:
                            flag_row = False
                        else:
                            row[i] = '{0: .5f}'.format(value)
                    elif self.celltype[i] == 'x':
                        try:
                            value = int(row[i])
                        except TypeError:
                            if row[i] == None:
                                row[i] = str(0)
                        except ValueError:
                            flag_row = False
                        else:
                            row[i] = str(value)
                if str(row[7]) == 'None': #dataflag validation
                    row[7] = 0
                else:
                    try:
                        float(row[7])
                        is_float = True
                    except:
                        flag_row = False
                        is_float = False
                    if is_float:
                        if float(row[7]) != 0:
                            dataflag = str(row[7]).split('.')
                            if len(dataflag[0]) != 5 or len(dataflag[1]) != 1:
                                flag_row = False
                            if len(dataflag) == 1:
                                flag_row = True
                row[1] = False if flag_row else True
                flag_all &= flag_row
        if flag_all:
            self.rod_main.Table_Validated(self.filetype)
            self.rod_main.Update_Status('{0} table validation completed!'.format(self.filetype.lower()), 'green')
            temp_filename = 'temp.{0}'.format(self.filetype.lower())
            self.write_file(temp_filename)
            self.rod_main.command_callback(None,'read {0} temp'.format(self.filetype.lower()))
            #os.remove(temp_filename)  # to remove the temp file
            self.rod_main.Update_Status('Valid {0} file'.format(self.filetype.lower()), 'Green')
            return 1
        else:
            self.rod_main.Update_Status('Validation Failed.', 'red')
            return 0
            
            
    def write_file(self, filename):
        '''save files to pointed dir'''
        
        fout = open(filename, 'w')
        if len(self.Comment_Entry.get_text()) > 0:
            fout.write(self.Comment_Entry.get_text().rstrip(os.linesep) + os.linesep)
        if self.filetype in ['Bul', 'Sur', 'Fit']:
            fout.write('{0} {1} {2} {3} {4} {5}'.format(self.Lattice_lat0_Entry.get_text(),\
                                                          self.Lattice_lat1_Entry.get_text(),\
                                                          self.Lattice_lat2_Entry.get_text(),\
                                                          self.Lattice_lat3_Entry.get_text(),\
                                                          self.Lattice_lat4_Entry.get_text(),\
                                                          self.Lattice_lat5_Entry.get_text()) + os.linesep) 
        elif self.filetype == 'Par':
            fout.write('set par' + os.linesep)
        for row in self.Table_ListStore:
            if self.filetype == 'Par': 
                newline = ''
                for i in (2,3,4):
                    value = eval('"{0:'+'{0}'.format(self.printwidth[i])+'}".format(row[i])')
                    newline = newline + value + ' ' 
                if row[8] == False:
                    newline = newline + '{0:10.5f}'.format(0.) + ' ' # 5
                    newline = newline + '{0:10.5f}'.format(0.) + ' ' # 6
                else:
                    newline = newline + eval('"{0:'+'{0}'.format(self.printwidth[5])+'}".format(row[5])') + ' '
                    newline = newline + eval('"{0:'+'{0}'.format(self.printwidth[6])+'}".format(row[6])') + ' '
                newline = newline + 'NO' if str(row[7]) == 'True' else newline + 'YES'
                fout.write(newline + os.linesep)
            elif self.filetype in ['Fit','Sur','Bul','Dat']:
                newline = ''
                for i in range(2, len(row)):
                    value = eval('"{0:'+'{0}'.format(self.printwidth[i])+'}".format(row[i])')
                    newline = newline + value + " "
                fout.write(newline[:-2] + os.linesep)
        if self.filetype == 'Par':
            fout.write('return return' + os.linesep)
        fout.close()
            
        
    def read_file(self, filename):# first read file to ROD then read file to CYROD
        '''Read different type of files and validate at the end'''
        
        if self.filetype == 'Par':# read file to rod
            if self.rod_main.valid_flag['Fit'] != 1:
                self.rod_main.Update_Status(' Read a valid Fit file first ', 'red')
                return
        self.Table_ListStore.clear()
        fin = open(filename, 'r')
        cnt = 0
        newline = fin.readline()
        self.Comment_Entry.set_text(newline) # comment line
        if self.filetype in ['Bul', 'Sur', 'Fit']:
            newline = fin.readline()
            items = newline.split() # lattice parameters
            for i in range(6):
                exec("self.Lattice_lat{0}_Entry.set_text(\'{1:.4f}\')".format(i, float(items[i])))
        elif self.filetype == 'Par':
            newline = fin.readline()
            while newline[:7] != 'set par':
                newline = fin.readline()
            self.Table_ListStore.append([1]+[0]+['scale']+['']+['1.0000']+['0.0000']*2+[0]*2+[None]*2+[0]*3)
            self.Table_ListStore.append([2]+[0]+['beta']+['']+['0.0000']+['0.0000']*2+[0]*2+[None]*2+[0]*3)
            self.Table_ListStore.append([3]+[0]+['surffrac']+['']+['1.0000']+['0.0000']*2+[0]*2+[None]*2+[0]*3)
            cnt = 3
        newline = fin.readline()
        while (newline != '\n' and newline != '' and newline[:6] != 'return' and len(newline) > 4):
            items = newline.split()
            if self.filetype == 'Par':
                items[-1] = True if items[-1] == 'YES' else False
            if items[0] in ('scale', 'beta', 'surffrac'):
                row = ('scale', 'beta', 'surffrac').index(items[0])
            else:
                self.Table_ListStore.append()
                cnt += 1
            for i in range(len(items)):
                if self.filetype == 'Par' and items[0] in ('scale', 'beta', 'surffrac'):
                    if i:
                        self.Table_ListStore[row][i+3] = items[i]
                    continue
                self.Table_ListStore[-1][0] = cnt
                self.Table_ListStore[-1][1] = 0
                self.Table_ListStore[-1][i+2] = items[i]
            newline = fin.readline()
            
        self.par_cnt = cnt
        self.rod_main.Table_Modified(self.filetype)
        fin.close()
        if self.Validate_CellValues(None) == 0:
            self.rod_main.Update_Status('{0}.{1} is not a valid file '.format(filename.split(".")[0],self.filetype.lower()),'red')
        elif self.filetype == 'Fit' or self.filetype == 'Par':
            try:
                self.rod_main.Table_Par.init_par()
                self.rod_main.Update_Status('Par file has been updated','green')
            except AttributeError:
                self.rod_main.Update_Status('Par file has been updated (hidden)','green')
                
                
    def insert_line(self, widget):
        
        if self.filetype == 'Bul' or self.filetype == 'Sur':
            emptyline = [0]*2 + ['']*5
        elif self.filetype == 'Fit':
            emptyline = [0]*2 + ['']*19
        elif self.filetype == 'Dat':
            emptyline = [0]*2 + ['']*6
        treemodel, pathlist = self.Table_TreeView.get_selection().get_selected_rows()
        # insert after selected row or if no row selected after the last row
        if len(pathlist):
            path = sorted(pathlist)[-1][0]+1
            emptyline[0] = path+1
            for i in range(path, len(self.Table_ListStore)):
                self.Table_ListStore[i][0] = self.Table_ListStore[i][0] + 1
            self.Table_ListStore.insert(path, emptyline)
        else:
            path = len(self.Table_ListStore)
            emptyline[0] = path+1
            self.Table_ListStore.insert(path,  emptyline)
        self.rod_main.Table_Modified(self.filetype)
        
        
    def delete_line(self, widget):
        
        treemodel, pathlist = self.Table_TreeView.get_selection().get_selected_rows()
        pathlist =  [path[0] for path in pathlist]
        if len(pathlist):
            for i in range(len(self.Table_ListStore)-1, sorted(pathlist)[0]-1, -1):
                if i in pathlist:
                    self.Table_ListStore.remove(self.Table_ListStore.get_iter(i))
                else:
                    self.Table_ListStore[i][0] = self.Table_ListStore[i][0] - sum(path<i for path in pathlist)
            self.rod_main.Table_Modified(self.filetype)
            
            
    def copy_line(self, widget):
        
        self.Table_Clipboard = []
        treemodel, pathlist = self.Table_TreeView.get_selection().get_selected_rows()
        if len(pathlist):
            for path in pathlist:
                row_data = []
                for value in self.Table_ListStore[path]:
                    row_data += [value]
                self.Table_Clipboard += [row_data]
                
                
    def paste_line(self, widget):
        
        treemodel, pathlist = self.Table_TreeView.get_selection().get_selected_rows()
        # paste on existing data and if no enough rows new rows will be created
        # if one desires to append to the end of the table, a new row has to be created manually first
        if len(pathlist):
            path = sorted(pathlist)[0][0]
            for i in range(path+len(self.Table_Clipboard)-len(self.Table_ListStore)):
                self.Table_ListStore.append()
            for i in range(len(self.Table_Clipboard)):
                self.Table_ListStore[path+i] = self.Table_Clipboard[i]
                self.Table_ListStore[path+i][0] = path+i+1
            self.rod_main.Table_Modified(self.filetype)
            
            
    def TableCell_toggle_toggleall(self, cellrenderertoggle, i):
        
        state = self.Table_ListStore[0][i]
        for row in self.Table_ListStore:
            row[i] = not state
        self.rod_main.Table_Modified(self.filetype)
        
        
    def TableCell_toggle_toggled(self, cellrenderertoggle, path, i):
        
        self.Table_TreeView.get_selection().unselect_all()
        self.Table_ListStore[path][i] = not self.Table_ListStore[path][i]
        self.rod_main.Table_Modified(self.filetype)
        
        
    def TableCell_edited(self, cellrenderertoggle, path, info, i):
        
        self.Table_TreeView.get_selection().unselect_all()
        self.Table_ListStore[path][i] = info
        self.rod_main.Table_Modified(self.filetype)
        
        
    def FileDialog_Construction(self, widget, OP_TYPE):
        
        if OP_TYPE == 0: # read file
            FileDialog = gtk.FileChooserDialog(title = "Choose File Name", action = gtk.FILE_CHOOSER_ACTION_OPEN,\
                        buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OPEN, gtk.RESPONSE_OK))
        elif OP_TYPE == 1: # write file
            FileDialog = gtk.FileChooserDialog(title = "Choose File Name", action = gtk.FILE_CHOOSER_ACTION_SAVE,\
                        buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_SAVE, gtk.RESPONSE_OK))
        filter = gtk.FileFilter()
        filter.set_name(self.filetype)
        filter.add_pattern('*.'+self.filetype.lower())
        FileDialog.add_filter(filter)
        filter = gtk.FileFilter()
        filter.set_name("All Files")
        filter.add_pattern('*')
        FileDialog.add_filter(filter)
        response = FileDialog.run()
        if response == gtk.RESPONSE_OK:
            if OP_TYPE == 0:
                self.read_file(FileDialog.get_filename())
            elif OP_TYPE == 1:
                self.write_file(FileDialog.get_filename())
        FileDialog.destroy()
    
    
    def init_par(self):
        '''init_par initilize Par files when a fit file readed'''
        
        self.Table_ListStore.clear()
        self.Table_ListStore.append([1]+[0]+['scale']+['']+['1.0000']+['0.0000']*2+[0]*2+[None]*2+[0]*3)
        self.Table_ListStore.append([2]+[0]+['beta']+['']+['0.0000']+['0.0000']*2+[0]*2+[None]*2+[0]*3)
        self.Table_ListStore.append([3]+[0]+['surffrac']+['']+['1.0000']+['0.0000']*2+[0]*2+[None]*2+[0]*3)
        count_N = 1
        NDISTOT = self.rod_main.Get_Global_Variable('NDISTOT')
        NDWTOT = self.rod_main.Get_Global_Variable('NDWTOT')
        NDWTOT2 =self.rod_main.Get_Global_Variable('NDWTOT2')
        NOCCTOT = self.rod_main.Get_Global_Variable('NOCCTOT')
        NSUBSCALETOT = self.rod_main.Get_Global_Variable('NSUBSCALETOT')
        NMOLECULES = self.rod_main.Get_Global_Variable('NMOLECULES')
        list_partot = (NDISTOT,NDWTOT,NDWTOT2,NOCCTOT,NSUBSCALETOT,NMOLECULES)
        list_parname = ('displace','b1','b2','occupancy','subscale','molecule')
        list_pars = (('DISPL','DISPLLIM','DISPLFIT'),('DEBWAL','DEBWALLIM','DEBWALFIT'),('DEBWAL2','DEBWAL2LIM','DEBWAL2FIT'),
                    ('OCCUP','OCCUPLIM','OCCUPFIT'),('SUBSCALE','SUBSCALELIM','SUBSCALEFIT'),('MOLPAR','MOLPARLIM','MOLPARFIT'))
        sbs_list = (('SCALE','SCALELIM','SCALEFIT'),('BETA','BETALIM','BETAFIT'),('SURFFRAC','SURFFRACLIM','SURFFRACFIT'))
        for row in self.Table_ListStore:
            row[0] = count_N
            count_N += 1
            if row[2] in ('scale','beta','surffrac'):
                row_index = ('scale','beta','surffrac').index(row[2])
                exec('row[4] = self.rod_main.Get_Global_Variable(\'{0}\')'.format(sbs_list[row_index][0]))
                exec('row[5] = self.rod_main.Get_Global_Variable(\'{0}\')[0]'.format(sbs_list[row_index][1]))
                exec('row[6] = self.rod_main.Get_Global_Variable(\'{0}\')[1]'.format(sbs_list[row_index][1]))
                exec('row[7] = self.rod_main.Get_Global_Variable(\'{0}\')'.format(sbs_list[row_index][2]))
        for i in range(6):
            for j in range(list_partot[i]):
                self.Table_ListStore.append()
                self.Table_ListStore[-1][0] = count_N
                self.Table_ListStore[-1][1] = 0
                self.Table_ListStore[-1][2] = list_parname[i]
                self.Table_ListStore[-1][3] = j+1
                exec('self.Table_ListStore[-1][4] = self.rod_main.Get_Global_Variable(\'{0}\')[j]'.format(list_pars[i][0]))
                exec('self.Table_ListStore[-1][5] = self.rod_main.Get_Global_Variable(\'{0}\')[j][0]'.format(list_pars[i][1]))
                exec('self.Table_ListStore[-1][6] = self.rod_main.Get_Global_Variable(\'{0}\')[j][1]'.format(list_pars[i][1]))
                exec('self.Table_ListStore[-1][7] = self.rod_main.Get_Global_Variable(\'{0}\')[j]'.format(list_pars[i][2]))
                count_N += 1
        self.par_cnt = count_N - 1
        if self.Validate_CellValues(None) and self.filetype == 'Fit':
            self.rod_main.Update_Status('Par file has been initialized','green')
        else:
            self.rod_main.Update_Status('Par file initialization failed','red')
        
        
    def FIT_center(self,widget):
        '''Cen Button which will center selected parameters'''
        
        for row in self.Table_ListStore:
            row[8] = True
        self.Validate_CellValues(None)
        if self.rod_main.valid_flag['Fit'] and self.rod_main.valid_flag['Dat']:
            self.rod_main.command_callback(None,'fit acen ret')
            FITMAX = self.rod_main.Get_Global_Variable('FITMAX')[0:self.par_cnt]
            FITMIN = self.rod_main.Get_Global_Variable('FITMIN')[0:self.par_cnt]
            count = 0
            for row in self.Table_ListStore:
                row[4] = '{0:.5}'.format((FITMIN[count]+FITMAX[count])/2)
                count += 1
            self.Validate_CellValues(None)
        else:
            self.rod_main.Update_Status('Read both valid dat and fit files first!','red')
        
        
    def hide_window(self, widget, event, MainWindow):
        
        showflag_index = ('Bul','Sur','Fit','Par','Dat').index(self.filetype)
        exec("MainWindow.Table_{0:s}.Table_Window.hide()".format(self.filetype))
        MainWindow.Table_showflag[showflag_index] = False # this is currently used to pass the short cut methods, might be replaced with a better way
        # return False and gtk will emit the destroy signal
        exec('self.rod_main.Status_Bar_{0:s}_ToggleButton.set_active(False)'.format(self.filetype))
        return True
    
    
    def __init__(self, MainWindow, filetype):
        
        self.filetype = filetype
        self.rod_main = MainWindow  #rod.pyx
        
 ############################################### Toolbar###########################################
        Toolbar_Read_Button = gtk.Button(' read ')
        Toolbar_Read_Button.connect('clicked', self.FileDialog_Construction, 0)
        Toolbar_Save_Button = gtk.Button(' save ')
        Toolbar_Save_Button.connect('clicked', self.FileDialog_Construction, 1)
        Toolbar_Insert_Button = gtk.Button(' ins ')
        Toolbar_Insert_Button.connect('clicked', self.insert_line)
        Toolbar_Delete_Button = gtk.Button(' del ')
        Toolbar_Delete_Button.connect('clicked', self.delete_line)
        Toolbar_Copy_Button = gtk.Button(' copy ')
        Toolbar_Copy_Button.connect('clicked', self.copy_line)
        Toolbar_Paste_Button = gtk.Button(' paste ')
        Toolbar_Paste_Button.connect('clicked', self.paste_line)
        Toolbar_Validate_Button = gtk.Button(' valid ')
        Toolbar_Validate_Button.connect('clicked', self.Validate_CellValues)
        Toolbar_HBox = gtk.HBox(homogeneous = False, spacing = 3)
        Toolbar_HBox.set_border_width(3)
        Toolbar_HBox.pack_start(Toolbar_Read_Button, False, False, 0)
        Toolbar_HBox.pack_start(Toolbar_Save_Button, False, False, 0)
        if self.filetype != "Par":
            Toolbar_HBox.pack_start(Toolbar_Insert_Button, False, False, 0)
            Toolbar_HBox.pack_start(Toolbar_Delete_Button, False, False, 0)
            Toolbar_HBox.pack_start(Toolbar_Copy_Button, False, False, 0)
            Toolbar_HBox.pack_start(Toolbar_Paste_Button, False, False, 0)
            
        Toolbar_HBox.pack_start(Toolbar_Validate_Button, False, False, 0)
        # Comment Text
        self.Comment_Entry = gtk.Entry()
        # Lattice Parameters
        Lattice_HBox = gtk.HBox(homogeneous = False, spacing = 3)
        Lattice_HBox.set_border_width(3)
        self.Lattice_Label = gtk.Label(' LAT: ')
        Lattice_HBox.pack_start(self.Lattice_Label, False, False, 0)
        for i in range(6):
            exec("self.Lattice_lat{0}_Entry = gtk.Entry()".format(i))
            exec("self.Lattice_lat{0}_Entry.set_size_request(65,20)".format(i))
            exec("self.Lattice_lat{0}_Entry.set_alignment(0.5)".format(i))
            exec("Lattice_HBox.pack_start(self.Lattice_lat{0}_Entry, True, True, 0)".format(i))
            
#################################### ListStore Initialization #########################################
        if filetype == 'Bul' or filetype == 'Sur':
            self.celltype = ['nrow', 'flag', 'el', 'val', 'val', 'val', 'dw']
            self.Table_ListStore = gtk.ListStore(int, bool, str, str, str, str, str)
            #self.printwidth = ['3d', 'bool', '2s', ' 10.5f', ' 10.5f', ' 10.5f', '3d']
            self.printwidth = ['3d', 'bool', '2', '10', '10', '10', '3']
            cellwidth = [0, 0, 30, 55, 55, 55, 30]
            title = ['N', '', 'EL', 'X', 'Y', 'Z', 'NDW']
            expand = [False, False, False, True, True, True, False]
            ncol = 7
        elif filetype == 'Par':
            self.celltype = ['nrow', 'flag', 'tpar', 'ipar', 'val', 'val', 'val', 'flag', 'flag', 'val', 'val', 'flag', 'flag', 'flag']
            self.Table_ListStore = gtk.ListStore(int, bool, str, str, str, str, str, bool, bool, str, str, bool, bool, bool)
            #self.printwidth = ['3d', 'bool', '9s', '3d', '10.6f', '10.6f', '10.6f', 'bool', 'bool', '10.6f', '10.6f', 'bool', 'bool', 'bool']
            self.printwidth = ['3d', 'bool', '9', '3', '10', '10', '10', 'bool', 'bool', '10.6f', '10.6f', 'bool', 'bool', 'bool']
            #self.printwidth_par = ['10', '3', '9','9','9','5']
            cellwidth = [0, 0, 80, 30, 85, 85, 85, 0, 0, 85, 85, 0, 0, 0]
            title = ['N', '', 'PAR', '#', 'INIT', 'LOWER', 'UPPER', 'FIX', 'BD', 'FINAL', 'ERROR', '', '', '']
            expand = [False, False, False, False, True, True, True, False, False, True, True, False, False, False]
            Lattice_HBox.set_no_show_all(True)
            self.Table_ListStore.append([1]+[0]+['scale']+['']+['1.0000']+['0.0000']*2+[0]*2+[None]*2+[0]*3)
            self.Table_ListStore.append([2]+[0]+['beta']+['']+['0.0000']+['0.0000']*2+[0]*2+[None]*2+[0]*3)
            self.Table_ListStore.append([3]+[0]+['surffrac']+['']+['1.0000']+['0.0000']*2+[0]*2+[None]*2+[0]*3)
            ncol = 11
        elif filetype == 'Fit':
            self.celltype = ['nrow', 'flag', 'el', 'val', 'val', 'displ', 'val', 'displ',\
                                                   'val', 'val', 'displ', 'val', 'displ',\
                                                   'val', 'val', 'displ', 'val', 'displ',\
                                                   'dw', 'dw', 'occ']
            self.Table_ListStore = gtk.ListStore(int, bool, str, str, str, str, str, str,\
                                                                 str, str, str, str, str,\
                                                                 str, str, str, str, str,\
                                                                 str, str, str)
            #self.printwidth = ['3d', 'bool', '2s', '10.5f', '10.5f', '3d', '10.5f', '3d',\
                                                   #'10.5f', '10.5f', '3d', '10.5f', '3d',\
                                                   #'10.5f', '10.5f', '3d', '10.5f', '3d',\
                                                   #'3d', '3d', '3d']
            self.printwidth = ['3d', 'bool', '2', '10', '10', '3', '10', '3',\
                                                   '10', '10', '3', '10', '3',\
                                                   '10', '10', '3', '10', '3',\
                                                   '3', '3', '3']
            cellwidth = [0, 0, 30, 65, 65, 30, 65, 30,\
                                   65, 65, 30, 65, 65,\
                                   65, 65, 30, 65, 30,\
                                   30, 30, 30]
            title = ['N', '', 'EL', 'X0', 'CX1', 'NX1', 'CX2', 'NX2',\
                                    'Y0', 'CY1', 'NY1', 'CY2', 'NY2',\
                                    'Z0', 'CZ1', 'NZ1', 'CZ2', 'NZ2',\
                                    'NDW1', 'NDW2', 'NOCC']
            expand = [False, False, False, True, False, False, False, False,\
                                           True, False, False, False, False,\
                                           True, False, False, False, False,\
                                           False, False, False]
            ncol = 21
        elif filetype == 'Dat':
            self.celltype = ['nrow', 'flag', 'HK', 'HK', 'L', 'F', 'F', 'x']
            self.Table_ListStore = gtk.ListStore(int, bool, str, str, str, str, str, str)
            self.printwidth = ['3d','bool','3','3', '10','10','10','10']
            cellwidth = [0, 0, 30, 30, 65, 85, 75, 70]
            title = ['N', '', 'H', 'K', 'L', 'F', 'SIGMA', 'dataflag']
            expand = [False, False, False, False, False, True, True, False]
            Lattice_HBox.set_no_show_all(True)
            ncol = 8
            
##################################### Distribute data ##################################################
        self.Table_TreeView = gtk.TreeView(self.Table_ListStore)
        self.Table_TreeView.get_selection().set_mode(gtk.SELECTION_MULTIPLE)
        self.Table_TreeView.set_enable_search(False)
        self.Table_TreeView.set_grid_lines(gtk.TREE_VIEW_GRID_LINES_BOTH)
        Table_CellRendererText0 = gtk.CellRendererText()
        Table_CellRendererText0.set_property('xalign', 0.5)
        Table_CellRendererText0.set_property('editable', False)
        Table_CellRendererText0.set_property("cell-background", 'pink')
        Table_CellRendererText0.set_fixed_size(30, 20)
        Table_TreeViewColumn0 = gtk.TreeViewColumn('N', Table_CellRendererText0, text = 0, \
                                                                                 cell_background_set = 1)
        Table_TreeViewColumn0.set_alignment(0.5)
        Table_TreeViewColumn0.set_expand(False)
        Table_TreeViewColumn0.set_min_width(30)
        self.Table_TreeView.append_column(Table_TreeViewColumn0)
        for i in range(2, ncol):
            dummycellrenderertext = gtk.CellRendererText()
            dummycellrenderertext.set_property('xalign', 0.5)
            dummycellrenderertext.set_property('editable', True)
            dummycellrenderertext.set_fixed_size(cellwidth[i], 20)
            dummycellrenderertext.connect('edited', self.TableCell_edited, i)
            dummytreeviewcolumn = gtk.TreeViewColumn(title[i], dummycellrenderertext, text = i)
            dummytreeviewcolumn.set_alignment(0.5)
            dummytreeviewcolumn.set_expand(expand[i])
            dummytreeviewcolumn.set_min_width(cellwidth[i])
            self.Table_TreeView.append_column(dummytreeviewcolumn) # this is how to append a big deal of data to treeview
        if filetype == 'Par':
            self.Table_TreeView.get_column(1).get_cell_renderers()[0].set_property('editable', False)
            self.Table_TreeView.get_column(2).get_cell_renderers()[0].set_property('editable', False)
            self.Table_TreeView.get_column(8).get_cell_renderers()[0].set_property('editable', False)
            self.Table_TreeView.get_column(9).get_cell_renderers()[0].set_property('editable', False)
            self.Table_TreeView.get_column(4).get_cell_renderers()[0].set_property("cell-background", 'pink')
            # self.Table_TreeView.get_column(3).get_cell_renderers()[0].set_property("cell_background_set", False)
            self.Table_TreeView.get_column(4).add_attribute(self.Table_TreeView.get_column(4).\
                                                            get_cell_renderers()[0], "cell_background_set", 11)
            self.Table_TreeView.get_column(5).get_cell_renderers()[0].set_property("cell-background", 'pink')
            self.Table_TreeView.get_column(5).add_attribute(self.Table_TreeView.get_column(5).\
                                                            get_cell_renderers()[0], "cell_background_set", 12)
            dummycellrenderertoggle = gtk.CellRendererToggle()
            dummycellrenderertoggle.connect('toggled', self.TableCell_toggle_toggled, 7)
            dummytreeviewcolumn = gtk.TreeViewColumn(title[7], dummycellrenderertoggle, active = 7)
            dummytreeviewcolumn.set_clickable(True)
            dummytreeviewcolumn.connect('clicked', self.TableCell_toggle_toggleall, 7)
            dummytreeviewcolumn.set_alignment(0.5)
            self.Table_TreeView.remove_column(self.Table_TreeView.get_column(6))
            self.Table_TreeView.insert_column(dummytreeviewcolumn, 6)
            dummycellrenderertoggle = gtk.CellRendererToggle()
            dummycellrenderertoggle.connect('toggled', self.TableCell_toggle_toggled, 8)
            dummytreeviewcolumn = gtk.TreeViewColumn(title[8], dummycellrenderertoggle, active = 8)
            dummytreeviewcolumn.set_clickable(True)
            dummytreeviewcolumn.connect('clicked', self.TableCell_toggle_toggleall, 8)
            dummytreeviewcolumn.set_alignment(0.5)
            self.Table_TreeView.remove_column(self.Table_TreeView.get_column(7))
            self.Table_TreeView.insert_column(dummytreeviewcolumn, 7)
            self.Table_TreeView.get_column(9).get_cell_renderers()[0].set_property("cell-background", 'pink')
            self.Table_TreeView.get_column(9).add_attribute(self.Table_TreeView.get_column(9).\
                                                             get_cell_renderers()[0], "cell_background_set", 13)
        Table_ScrolledWindow = gtk.ScrolledWindow()
        Table_ScrolledWindow.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        Table_ScrolledWindow.add_with_viewport(self.Table_TreeView)
        if self.filetype == 'Par':
            Results_HBox = gtk.HBox(homogeneous = False, spacing = 3)
            for i in ('chisqr','normalized','Q','R_factor'):    # R-factor in fit.c 305 no global variable (need to calculate manually)
                exec('self.{0}_Label = gtk.Label(\'{0}:\')'.format(i)) 
                exec('self.{0}_Entry = gtk.Entry()'.format(i))
                exec('self.{0}_Entry.set_sensitive(False)'.format(i))
                exec('self.{0}_Entry.set_size_request(95,20)'.format(i))
                exec('Results_HBox.pack_start(self.{0}_Label, False, False, 0)'.format(i))
                exec('Results_HBox.pack_start(self.{0}_Entry, False, False, 0)'.format(i))
            FIT_center_Button = gtk.Button(' cen ')
            FIT_center_Button.connect('clicked',self.FIT_center)
            Toolbar_HBox.pack_start(FIT_center_Button,False,False,0)
            if self.rod_main.valid_flag['Fit'] != -1:
                self.init_par()
            
######################################### VBox ###############################################
        self.Table_Window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        Main_Menu = MainWindow.Menu_ItemFactory(self.Table_Window)
        Main_Menu.set_visible(False)
        Table_VBox = gtk.VBox(homogeneous = False, spacing = 3)
        Table_VBox.set_border_width(3)
        Table_VBox.pack_start(Main_Menu, False, False, 0)
        Table_VBox.pack_start(Toolbar_HBox, False, False, 0)
        Table_VBox.pack_start(self.Comment_Entry, False, False, 0)
        Table_VBox.pack_start(Lattice_HBox, False, False, 0)
        Table_VBox.pack_start(Table_ScrolledWindow, True, True, 0)
        if self.filetype == 'Par':
            Table_VBox.pack_start(Results_HBox, False, False, 0)
            
########################################## Window ###############################################
        self.Table_Window.connect("delete_event", self.hide_window, MainWindow)
        self.Table_Window.set_title(filetype)
        self.Table_Window.add(Table_VBox)
        self.Table_Window.show_all()