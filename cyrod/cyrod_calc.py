# -*- coding: utf-8 -*-
####################################################################################
####    CyRod - Python Wrapper for rod                                          ####
####    Email to nrszhou@gmail.com for any questions or comments                ####
####    Version : 1.0.0 (Feb. 2016)                                             ####
####    Project site: wait....                                                  ####
####################################################################################

import gtk
import pygtk
import numpy
from math import *
import numpy

def cal_rod(widget,rod_main,h_Entry,k_Entry):

    h = h_Entry.get_text()
    k = k_Entry.get_text()
    try:
        float(h)
        float(k)
    except ValueError:
        rod_main.Update_Status('Wrong type input: {0} or {1}'.format(h,k),'red')
        return
    rod_main.command_callback(None,'cal rod {0} {1}'.format(h,k))
    
    
def refresh_step(rod_main):
    
    for i in ('hstep','kstep'):
        for j in ('qrange','range'):
            exec('rod_main.calc_{0}_{1}_Entry.set_text(rod_main.{1})'.format(j,i))
    return
    
    
def cal_range(widget,rod_main):
    
    
    calc_range_Entry_tuple = ('hstart','hend','hstep','kstart','kend','kstep','l_index')
    for i in range(7):
        exec('rod_main.{0} = rod_main.calc_range_{0}_Entry.get_text()'.format(calc_range_Entry_tuple[i]))
        try:
            exec('float((rod_main.{0})'.format(calc_range_Entry_tuple[i]))
        except:
            rod_main.Update_Status('Wrong input, please check again','red')
    rod_main.command_callback(None,'cal range {0} {1} {2} {3} {4} {5} {6} '.\
        format(rod_main.hstart,rod_main.hend,rod_main.hstep,rod_main.kstart,rod_main.kend,rod_main.kstep,rod_main.l_index))
    refresh_step(rod_main)
    
    
def cal_qrange(widget,rod_main):
    
    
    calc_qrange_Entry_tuple = ('q_max','hstep','kstep','l_index')
    for i in range(4):
        exec('rod_main.{0} = rod_main.calc_qrange_{0}_Entry.get_text()'.format(calc_qrange_Entry_tuple[i]))
        try:
            exec('float((rod_main.{0})'.format(calc_qrange_Entry_tuple[i]))
            exec('rod_main.calc_qrange_{0}_Entry.set_text(\'{:.2}\')'.format(calc_qrange_Entry_tuple[i],'float(rod_main.{0})'.format(calc_qrange_Entry_tuple[i])))
        except:
            rod_main.Update_Status('Wrong input, please check again','red')
    rod_main.command_callback(None,'cal qrange {0} {1} {2} {3}'.\
        format(rod_main.q_max,rod_main.hstep,rod_main.kstep,rod_main.l_index))
    refresh_step(rod_main)
    
    
def cal_data(widget,rod_main):
    
    rod_main.command_callback(None,'cal data')
 
 
############################# not in GUI now ###################################
def cal_distance(rod_main,atom_1,atom_2):
    
    rod_main.command_callback(None,'cal dist {0} {1}'.format(atom_1,atom_2))


def cal_angle(rod_main,atom_1,atom_center,atom_3):
    
    rod_main.command_callback(None,'cal angle {0} {1} {2}'.format(atom_1,atom_center,atom_3))
    
    
def cal_roughness(rod_main):
    
    rod_main.command_callback(None,'cal rough')
