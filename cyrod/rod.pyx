# -*- coding: utf-8 -*-

####################################################################################
####    CyRod - Python Wrapper for ROD                                          #### 
####    Email to nrszhou@gmail.com for any questions or comments                ####
####    Version : 1.0.0 (Feb. 2016)                                             ####
####    Project site: wait....                                                  ####
####################################################################################
'''
work to be done:
1. menu factory need to be modified to connect Table_Visibility_Toggled
2. finish the command guardian
3. design Plot_Window
4. make cyrod_calc
5. some other stuff (debug..)
'''

import pygtk
import gtk
import numpy
import os
from math import cos, sin,fabs
import time
import cyrod_read
reload(cyrod_read)
import cyrod_plot
reload(cyrod_plot)
import cyrod_set
reload(cyrod_set)
import cyrod_calc
reload(cyrod_calc)
import cyrod_menu
reload(cyrod_menu)
#import subprocess
#import glib
# FOR A TEST here


class MyMainWindow:

    valid_flag = {'Bul':-1, 'Sur':-1, 'Fit':-1, 'Dat':-1, 'Par':-1}
    # -1 : none, 0: modified, 1: validated 
    
    def fit_parameter_changed(self,widget,widget_type,varname = None):
        '''Get data from GUI and then send them to ROD'''
        
        if widget_type =='combobox':
            self.command_callback(None,'fit control {0} ret ret '.format(widget.get_active_text()))
        elif widget_type == 'Entry':
            self.command_callback(None,'fit control {0} {1} ret ret'.format(varname,widget.get_text()))
    
    def command_callback(self, widget, py_string = None):
        '''Pass different commands to ROD'''

        cdef bytes py_bytes        
        cdef char* c_string
        
        if py_string == None:
            py_string = self.CommandEntry.get_text()
        items = py_string.split()
        if len(items) == 0:
            self.Update_Status(' No valid commands ', 'red')
            return 0
        len0 = len(items[0])
        if items[0] == 'plot'[:len0]:
            self.Plot_Visibility_Toggled('plot')
            self.CommandEntry.set_text('')
            cyrod_menu.display_menu(self,cyrod_menu.get_menu('main_menu'))
            return
        [state,msg,color] = cyrod_menu.command_parser(items,self)
        if state:
            self.Update_Status(msg,color)
            py_bytes = (py_string[len0+1:]+' ret ret').encode()
            c_string = py_bytes
            put_command(c_string)
            if items[0] == "read"[:len0]:
                read_type()
                self.CommandEntry.set_text('')
                self.CommandEntry.set_position(-1)
            elif items[0] == "calc"[:len0]:           
                calculate()
                self.CommandEntry.set_text('calc ')
                self.CommandEntry.set_position(-1)
            elif items[0] == "reset"[:len0]:
                initialize()
                self.CommandEntry.set_text('')
            elif items[0] == "fit"[:len0]:           
                fit()
                self.CommandEntry.set_text('fit ')
                self.CommandEntry.set_position(-1)
            elif items[0] == "fitplus"[:len0]: 
                fitplus()
                self.CommandEntry.set_text('')
                self.CommandEntry.set_position(-1)
            elif items[0] == "list"[:len0]:           
                list(0)
                self.CommandEntry.set_text('')
                self.CommandEntry.set_position(-1)
            elif items[0] == "set"[:len0]:           
                set()
                self.CommandEntry.set_text('set ')
                self.CommandEntry.set_position(-1)
            elif items[0] == "quit"[:len0]:          
                self.MainWindow_Destroy(None, None)
        else:
            #self.CommandEntry.set_text('')
            self.Update_Status(msg,color)
            

    def Update_Status(self, text, color):  # revamping status updating function

        self.Status_Bar_Label.set_markup('<span color=\"{0}\">  {1}  </span>'.format(color, text))
        

    def Table_Modified(self, filetype):   # flag changed here to 0

        exec("self.Status_Bar_{0:s}_ToggleButton.get_child().set_markup(\'<span color=\"orange\"> {0} </span>\')".format(filetype))
        self.valid_flag[filetype] = 0


    def Table_Validated(self, filetype):   # flag changed here to 1

        exec("self.Status_Bar_{0:s}_ToggleButton.get_child().set_markup(\'<span color=\"green\"> {0} </span>\')".format(filetype))
        self.valid_flag[filetype] = 1


    def ItemFactory_Activated(self,widegt,filetype):
        
        exec('self.Status_Bar_{0}_ToggleButton.emit(\'activate\')'.format(filetype))
        
        
    def Get_Car(self,x,y,z):
        '''call get_cartesian function from util.c'''
        
        cdef float cycart[3]
        cdef float [:] cyCART = cycart
        get_cartesian(cycart,x, y,z)
        return numpy.array(cyCART)
        
        
    def Get_Global_Variable(self, varname):# here global variables pass can be achived in two was
        
        cdef float [:] cyDOMMAT11 = DOMMAT11
        cdef float [:] cyDOMMAT12 = DOMMAT12
        cdef float [:] cyDOMMAT21 = DOMMAT21
        cdef float [:] cyDOMMAT22 = DOMMAT22
        cdef float [:] cyDOMOCCUP = DOMOCCUP
        cdef char [:,:] cyELEMENT = ELEMENT
        cdef float [:,:] cyF_COEFF = F_COEFF
        
        #FIT
        cdef float [:] cyFITERR = FITERR
        cdef int [:]   cyFIXPAR = FIXPAR
        cdef char [:,:] cyFITTXT = FITTXT
        cdef float [:] cyFITMIN = FITMIN
        cdef float [:] cyFITMAX = FITMAX
        cdef float [:] cyFITPAR = FITPAR
        
        cdef float [:] cySCALELIM = SCALELIM
        cdef float [:] cyBETALIM = BETALIM
        cdef float [:] cySURFFRACLIM = SURFFRACLIM
        cdef float [:] cyDISPL = DISPL
        cdef float [:,:] cyDISPLLIM = DISPLLIM
        cdef int [:] cyDISPLFIT = DISPLFIT
        cdef float [:] cyDEBWAL = DEBWAL
        cdef float [:,:] cyDEBWALLIM = DEBWALLIM
        cdef int [:] cyDEBWALFIT= DEBWALFIT
        cdef float [:] cyDEBWAL2 = DEBWAL2
        cdef float [:,:] cyDEBWAL2LIM = DEBWAL2LIM
        cdef int [:] cyDEBWAL2FIT = DEBWAL2FIT
        cdef float [:] cyOCCUP = OCCUP
        cdef float [:,:] cyOCCUPLIM = OCCUPLIM
        cdef int [:] cyOCCUPFIT = OCCUPFIT
        cdef float [:] cySUBSCALE = SUBSCALE
        cdef float [:,:] cySUBSCALELIM = SUBSCALELIM
        cdef float [:] cySUBSCALEFIT = SUBSCALEFIT
        cdef float [:,:] cyMOLPAR = MOLPAR
        cdef float [:,:,:] cyMOLPARLIM = MOLPARLIM
        cdef int [:,:] cyMOLPARFIT = MOLPARFIT
        
        #PLOT
        cdef float [:] cyXS = XS
        cdef float [:] cyYS = YS
        cdef float [:] cyZS = ZS
        cdef float [:] cyXSFIT = XSFIT
        cdef float [:] cyYSFIT = YSFIT
        cdef float [:] cyZSFIT = ZSFIT
        cdef float [:] cyDLAT = DLAT
        cdef int [:] cyTS = TS
        cdef float [:] cyATRAD = ATRAD
        
        cdef float [:] cyHDAT = HDAT
        cdef float [:] cyKDAT = KDAT
        cdef float [:] cyERRDAT = ERRDAT
        cdef float [:] cyLDAT = LDAT
        cdef float [:] cyHTH = HTH
        cdef float [:] cyKTH = KTH
        cdef float [:] cyLTH = LTH
        cdef float [:] cyFDAT = FDAT
        cdef float [:,:] cyFTH = FTH
        cdef float [:] cyRLAT = RLAT
        
        
        global NDAT,NTH,HROD,KROD
        global LSTART, LEND, STRUCFAC,NL,ATTEN,BETA,LBRAGG,FRACFLAG,NENERGY,NLAYERS,SCALE,NSUBSCALE,SURFFRAC,\
              SURF2FRAC,NSURF2,L_INTERVAL,ERROR_FRACTION,ROUGHMODEL # set_cal
        global DOMEQUAL,ZEROFRACT,COHERENTDOMAINS,NDOMAIN,FITDOMOCC #set_domain
        global NBULK,NSURF
        global CHISQR #FIT
        global SCALE,SCALEFIT,BETA,BETAFIT,SURFFRAC,SURFFRACFIT,NDISTOT,NDWTOT,NDWTOT2,NOCCTOT,NSUBSCALETOT,NMOLECULES
        global ALPHA_KEAT,BETA_KEAT   # in keating (energy())
        #global LSQ_ERRORCALC #Fit
        if varname == 'ROUGHMODEL':
            if ROUGHMODEL == 0:
                var = ROUGHMODEL # NEED A CONVERSION HERE
            elif ROUGHMODEL >= 2:
                var = ROUGHMODEL - 1
        elif varname in ('DOMMAT11','DOMMAT12','DOMMAT21','DOMMAT22','DOMOCCUP'):
            var = eval('numpy.array(cy{0})[0:8]'.format(varname))
            #exec('var = cy{0}[0:NDOMAIN]'.format(varname))
        elif varname in ('FITPAR','FITERR','FIXPAR','FITMAX','FITMIN'):  # get_global_variable for FIT
            var = eval('numpy.array(cy{0})'.format(varname))
        elif varname in ('XS','YS','ZS','XSFIT','YSFIT','ZSFIT','DLAT','KDAT','HDAT',\
                        'TS','ATRAD','HTH','KTH','LTH','FDAT','FTH','RLAT','LDAT','ERRDAT'): #for plotting
            var = eval('numpy.array(cy{0})'.format(varname))
        elif varname in ('SCALELIM','BETALIM','SURFFRACLIM','DISPL','DISPLLIM','DISPLFIT','DEBWAL',
                         'DEBWALLIM','DEBWALFIT','DEBWAL2','DEBWAL2LIM','DEBWAL2FIT','OCCUP','OCCUPLIM','OCCUPFIT','MOLPAR'
                        ,'MOLPARLIM','MOLPARFIT','SUBSCALE','SUBSCALELIM','SUBSCALEFIT'):  # for init_par in cyrod_read
            var = eval('numpy.array(cy{0})'.format(varname))
        elif varname in ('XS','YS','ZS','XSFIT','YSFIT','ZSFIT'): #plot
            var = eval('numpy.array(cy{0})'.format(varname))
        elif varname in ('ELEMENT'):
            var = eval('numpy.array(cy{0})'.format(varname))
        else:
            var = eval('{0}'.format(varname))
        
#global value trasfer (set_fatomic)
        '''elif varname == 'set_fatomic_ELEMENT':
            middle_var = []
            for i in range(NTYPES):
                middle_var.append('{}{}'.format(chr(cyELEMENT[i][0]),chr(cyELEMENT[i][1])))
            var = middle_var
        elif varname == 'set_fatomic_F_COEFF':
            print_flag = False
            middle_var = []
            for i in range(NTYPES):
                middle_var.append(numpy.array(cyF_COEFF[i][:]))
            var = middle_var'''
        return var
  

    def Table_Visibility_Toggled(self, widget, filetype):

        #try to make the itemfactory hidable
        try:
            filetypes = ('Bul','Sur','Fit','Par','Dat')
            widget_mode = int(widget)
            filetype = filetypes[widget_mode-1]
            if self.Table_showflag[widget_mode-1] == True:
                exec("self.Table_{0:s}.Table_Window.hide()".format(filetype))
                self.Table_showflag[widget_mode-1] = False
                return
        except:
            widget_mode = widget.get_active()
        showflag_index = ('Bul','Sur','Fit','Par','Dat').index(filetype)
        if widget_mode:
            try:
                exec("self.Table_{0:s}.Table_Window.show()".format(filetype))
                self.Table_showflag[showflag_index] = True
                self.Update_Status('{0} Table is shown'.format(filetype), 'grey')
            except AttributeError:
                # create a new one
                exec("self.Table_{0:s} = cyrod_read.MyTableWindow(self, \'{0:s}\')".format(filetype))
                exec("self.Status_Bar_{0:s}_ToggleButton.get_child().set_markup(\"<span color='orange'> {0:s} </span>\")".format(filetype))
                exec("self.Table_{0:s}.Table_Window.resize(200, 400)".format(filetype))
                self.Table_showflag[showflag_index] = True
                self.Update_Status('New {0} Table is created'.format(filetype), 'blue')
        else:
            exec("self.Table_{0:s}.Table_Window.hide()".format(filetype))
            self.Update_Status('{0} Table is hidden'.format(filetype), 'grey')
            
            
    def Set_Visibility_Toggled(self, widget):

        # I know this part is not readable but it keeps it short
        if widget.get_active():
            try:
                self.Set_Notebook.Set_Window.show()
            except AttributeError:
                # create a new one
                self.Set_Notebook = cyrod_set.SetNotebookWindow(self)
                #self.Set_Notebook.Set_Window.resize(400, 500)
        else:
            self.Set_Notebook.Set_Window.hide()
            
    def Plot_Visibility_Toggled(self, widget):

        # I know this part is not readable but it keeps it short
        if widget == 'plot'or widget.get_active():
            try:
                self.Plot_Notebook.Plot_Window.show()
            except AttributeError:
                # create a new one
                self.Plot_Notebook = cyrod_plot.MyPlotWindow(self)
                #self.Fit_Notebook.Fit_Window.resize(400, 500)
        else:
            self.Plot_Notebook.Plot_Window.hide()
            
         
    def MainWindow_Destroy(self, widget, dummy = None): 

        for filetype in ['Bul', 'Sur', 'Fit', 'Par', 'Dat']:
            try:
                exec("self.Table_{0:s}.Table_Window.destroy()".format(filetype))
            except AttributeError:
                pass
        self.Main_Window.destroy()
        gtk.main_quit()
    
    
    def Ter_Visibility_Toggled(self,view_mode,widget):
        '''classic mode or modern mode'''
        if widget.get_active():
            if view_mode  == 1:
                self.Command_VBox.show()
                self.Main_sub_VBox.hide()
                self.Main_Window.resize(300,430)
            elif view_mode == 2:
                self.Command_VBox.hide()
                self.Main_sub_VBox.show()
                self.Main_Window.resize(325,270)
                
        
    def FIT_run(self,widget):
        
        #cdef float [:,:] cyNTH = FTH
        try:
            self.Table_Par.Validate_CellValues(None)
        except AttributeError:
            self.Update_Status('No Par window','red')
            return
        if not self.Table_Par.Validate_CellValues(None):
            self.Update_Status('Please validate Par file first!','red')
            return
        self.command_callback(None, 'fit run')
        FITPAR = self.Get_Global_Variable('FITPAR')[0:self.Table_Par.par_cnt]
        FITERR = self.Get_Global_Variable('FITERR')[0:self.Table_Par.par_cnt]
        FIXPAR = self.Get_Global_Variable('FIXPAR')[0:self.Table_Par.par_cnt]
        count = 0
        for row in self.Table_Par.Table_ListStore:
            if FIXPAR[count] == 0:
                row[9] ='{0:.6}'.format(FITPAR[count])
                row[10] = '{0:.6}'.format(FITERR[count])
            elif FIXPAR[count] == 1:
                row[9] = row[4]
                row[10] = 0
            count += 1
        chisqr = self.Get_Global_Variable('CHISQR')
        self.Table_Par.chisqr_Entry.set_text('{0:7.4f}'.format(chisqr))
        nfree = self.Table_Par.par_cnt - numpy.sum(self.Get_Global_Variable('FIXPAR'))
        NDAT = self.Get_Global_Variable('NDAT')
        self.Table_Par.normalized_Entry.set_text('{0:7.4f}'.format(chisqr/(NDAT-nfree+1e-10)))
        if NDAT - nfree >= 2:
            fit_Quality = gammq((NDAT - nfree-2)*0.5, chisqr*0.5)
        else:
            fit_Quality = 0.0
        fit_Quality = 0 if fit_Quality<1e-10 else fit_Quality
        self.Table_Par.Q_Entry.set_text('{0:.5f}'.format(fit_Quality))
        fth = self.Get_Global_Variable('FTH')[3]
        fdat = self.Get_Global_Variable('FDAT')
        ndat = self.Get_Global_Variable('NDAT')
        difsum = 1e-6
        fsum = 1e-6
        for i in range(ndat):
            difsum += fabs(FDAT[i]-fth[i])
            fsum += FDAT[i]
        self.Table_Par.R_factor_Entry.set_text('{0:.3f}'.format(difsum/fsum))
        self.Table_Par.show_fitresults()
        self.Table_Par.Validate_CellValues(None)
        self.Update_Status('FIT completed','green')
        

    def Menu_ItemFactory(self, window):

        accel_group = gtk.AccelGroup()

        # Param 1: The type of menu - can be MenuBar, Menu, or OptionMenu.
        # Param 2: The path of the menu.
        # Param 3: A reference to an AccelGroup. The item factory sets up
        # the accelerator table while generating menus.
        item_factory = gtk.ItemFactory(gtk.MenuBar, "<main>", accel_group)

        # This method generates the menu items. Pass to the item factory
        # the list of menu items
        item_factory.create_items(self.menu_items)

        # Attach the new accelerator group to the window.
        window.add_accel_group(accel_group)

        # need to keep a reference to item_factory to prevent its destruction
        self.item_factory = item_factory

        # Finally, return the actual menu bar created by the item factory.
        return item_factory.get_widget("<main>")


    def __init__(self):

        # Use Item Factory To Create Menu Items
        tooltips = gtk.Tooltips()
############################################# STATUS BAR ##################################################
# INCLUDE READ BUTTONS SET PLOT CALC BUTONS
        # Status Bar
        self.Table_showflag = []
        for filetype in ('Bul','Sur','Fit','Par','Dat'):
            self.Table_showflag.append(False)
        self.Status_Bar_Bul_ToggleButton = gtk.ToggleButton('')
        self.Status_Bar_Bul_ToggleButton.get_child().set_markup('<span color="#FE2E2E"> Bul </span>')
        self.Status_Bar_Bul_ToggleButton.connect('toggled', self.Table_Visibility_Toggled, "Bul")
        self.Status_Bar_Sur_ToggleButton = gtk.ToggleButton('')
        self.Status_Bar_Sur_ToggleButton.get_child().set_markup('<span color="#FE2E2E"> Sur </span>')
        self.Status_Bar_Sur_ToggleButton.connect('toggled', self.Table_Visibility_Toggled, "Sur")
        self.Status_Bar_Fit_ToggleButton = gtk.ToggleButton('')
        self.Status_Bar_Fit_ToggleButton.get_child().set_markup('<span color="#FE2E2E"> Fit </span>')
        self.Status_Bar_Fit_ToggleButton.connect('toggled', self.Table_Visibility_Toggled, "Fit")
        self.Status_Bar_Par_ToggleButton = gtk.ToggleButton('')
        self.Status_Bar_Par_ToggleButton.get_child().set_markup('<span color="#FE2E2E"> Par </span>')
        self.Status_Bar_Par_ToggleButton.connect('toggled', self.Table_Visibility_Toggled, "Par")
        self.Status_Bar_Dat_ToggleButton = gtk.ToggleButton('')
        self.Status_Bar_Dat_ToggleButton.get_child().set_markup('<span color="#FE2E2E"> Dat </span>')
        self.Status_Bar_Dat_ToggleButton.connect('toggled', self.Table_Visibility_Toggled, "Dat")
        self.Status_Bar_Set_ToggleButton = gtk.ToggleButton('')
        self.Status_Bar_Set_ToggleButton.get_child().set_markup('<span color="black"> Set </span>')
        self.Status_Bar_Set_ToggleButton.connect('toggled', self.Set_Visibility_Toggled)
        self.Status_Bar_Plot_ToggleButton = gtk.ToggleButton('')
        self.Status_Bar_Plot_ToggleButton.get_child().set_markup('<span color="black"> Plot </span>')
        self.Status_Bar_Plot_ToggleButton.connect('toggled', self.Plot_Visibility_Toggled)
        self.Status_Bar_Reset_ToggleButton = gtk.ToggleButton('')
        self.Status_Bar_Reset_ToggleButton.get_child().set_markup('<span color="black"> Reset </span>')
        self.Status_Bar_Reset_ToggleButton.connect('toggled', self.command_callback,'reset ')
#READ
        read_HBox = gtk.HBox(homogeneous = False,spacing = 2)
        read_HBox.pack_start(self.Status_Bar_Bul_ToggleButton,False,False,0)
        read_HBox.pack_start(self.Status_Bar_Sur_ToggleButton,False,False,0)
        read_HBox.pack_start(self.Status_Bar_Fit_ToggleButton,False,False,0)
        read_HBox.pack_start(self.Status_Bar_Par_ToggleButton,False,False,0)
        read_HBox.pack_start(self.Status_Bar_Dat_ToggleButton,False,False,0)
        read_frame = gtk.Frame(' READ ')
        tooltips.set_tip(read_frame,'Read files')
        read_frame.add(read_HBox)
        self.Status_Bar_Label = gtk.Label()
        self.Status_Bar_Label.set_alignment(0, 0)
        
        Status_Bar_HBox = gtk.HBox(homogeneous = False, spacing = 5)
        Status_Bar_HBox.set_border_width(0)
        Status_Bar_HBox.pack_start(read_frame,False, False, 0)
        Status_Bar_1_HBox = gtk.HBox(homogeneous = False, spacing = 5)
        Status_Bar_1_HBox.pack_start(self.Status_Bar_Set_ToggleButton,False,False,0)
        Status_Bar_1_HBox.pack_start(self.Status_Bar_Plot_ToggleButton,False,False,0)
        Status_Bar_1_HBox.pack_start(self.Status_Bar_Reset_ToggleButton,False,False,0)
        
############################################# FIT BUTTONS ##################################################
#FIT
        self.FIT_Button = gtk.Button('Fit')
        self.FIT_Button.set_size_request(55,25)
        self.FIT_Button.connect('clicked',self.FIT_run)
        
        Fit_control_estimate_Label = gtk.Label(' Err: ')
        tooltips.set_tip(Fit_control_estimate_Label,' Set error estimate method')
        self.Fit_control_estimate_ComboBox = gtk.combo_box_new_text()
        self.Fit_control_estimate_ComboBox.set_size_request(67,25)
        self.Fit_control_estimate_ComboBox.append_text('covar')
        self.Fit_control_estimate_ComboBox.append_text('chisqr')
        self.Fit_control_estimate_ComboBox.set_active(0)  #will be modified in the future
        self.Fit_control_estimate_ComboBox.connect('changed',self.fit_parameter_changed,'combobox')
        
        Fit_itermax_Label = gtk.Label(' Itermax: ')
        tooltips.set_tip(Fit_itermax_Label,' Set Max number of Iterations (LM fit) ')
        self.Fit_itermax_Entry = gtk.Entry()
        self.Fit_itermax_Entry.set_size_request(60,20)
        self.Fit_itermax_Entry.set_text('500')
        self.Fit_itermax_Entry.connect('activate',self.fit_parameter_changed,'Entry','itermax')
        
        Fit_convergence_Label = gtk.Label('ConvCri: ')
        tooltips.set_tip(Fit_convergence_Label,' Set the convergence criterion of CHisqr ')
        self.Fit_convergence_Entry = gtk.Entry()
        self.Fit_convergence_Entry.set_size_request(60,20)
        self.Fit_convergence_Entry.set_text('0.02')
        self.Fit_convergence_Entry.connect('activate',self.fit_parameter_changed,'Entry','conv')
        
        Fit_HBox = gtk.HBox(homogeneous = False,spacing = 2)
        Fit_HBox.pack_start(Fit_control_estimate_Label, False,False,0)
        Fit_HBox.pack_start(self.Fit_control_estimate_ComboBox, False,False,0)
        Fit_HBox.pack_start(Fit_itermax_Label, False,False,0)
        Fit_HBox.pack_start(self.Fit_itermax_Entry, False,False,0)
        Fit_HBox.pack_start(Fit_convergence_Label,False,False,0)
        Fit_HBox.pack_start(self.Fit_convergence_Entry,False,False,0)
        Fit_HBox.pack_start(self.FIT_Button, False, False,0)
        
        Fit_frame = gtk.Frame('FIT')
        Fit_frame.add(Fit_HBox)
        tooltips.set_tip(Fit_frame,'Start fit or change fit control parameters')

############################################# CALC BUTTONS ##################################################
        #calc_data
        self.calc_data_Button = gtk.Button('Calc_data')
        tooltips.set_tip(self.calc_data_Button,'Calculate f\'s for all data points')
        self.calc_data_Button.connect('clicked',cyrod_calc.cal_data,self)
        #rod
        calc_rod_h_Label = gtk.Label(' h: ')
        calc_rod_k_Label = gtk.Label(' k: ')
        calc_rod_h_Entry = gtk.Entry()
        calc_rod_h_Entry.set_size_request(50,20)
        calc_rod_h_Entry.set_text('1.0')
        calc_rod_k_Entry = gtk.Entry()
        calc_rod_k_Entry.set_size_request(50,20)
        calc_rod_k_Entry.set_text('1.0')
        self.calc_rod_Button = gtk.Button('Rod')
        tooltips.set_tip(self.calc_rod_Button,'Calculate rod profile')
        self.calc_rod_Button.set_size_request(40,25)
        self.calc_rod_Button.connect('clicked',cyrod_calc.cal_rod,self,calc_rod_h_Entry,calc_rod_k_Entry)
        calc_rod_HBox = gtk.HBox(homogeneous = False, spacing = 2)
        calc_rod_HBox.pack_start(self.calc_rod_Button,False,False,0)
        calc_rod_HBox.pack_start(calc_rod_h_Label,False,False,0)
        calc_rod_HBox.pack_start(calc_rod_h_Entry,False,False,0)
        calc_rod_HBox.pack_start(calc_rod_k_Label,False,False,0)
        calc_rod_HBox.pack_start(calc_rod_k_Entry,False,False,0)
        calc_rod_HBox.pack_end(self.calc_data_Button,False,False,0)
        #range
        calc_range_Entry_tuple = ('hstart','hend','hstep','kstart','kend','kstep','l_index')
        calc_range_Table = gtk.Table(1,9,homogeneous = False)
        self.calc_range_Button = gtk.Button('Range')
        tooltips.set_tip(self.calc_range_Button,'Calculate f\'s for range of h and k')
        self.calc_range_Button.connect('clicked',cyrod_calc.cal_range,self)
        self.calc_range_Button.set_size_request(42,25)
        calc_range_Table.attach(self.calc_range_Button,0,2,0,1)
        for i in range(7):
            exec('self.calc_range_{0}_Entry = gtk.Entry()'.format(calc_range_Entry_tuple[i]))
            exec('self.calc_range_{0}_Entry.set_size_request(40,25)'.format(calc_range_Entry_tuple[i]))
            exec('self.calc_range_{0}_Entry.set_text(\'0.00\')'.format(calc_range_Entry_tuple[i]))
            exec('tooltips.set_tip(self.calc_range_{0}_Entry,\'{0}\')'.format(calc_range_Entry_tuple[i]))
            exec('calc_range_Table.attach(self.calc_range_{0}_Entry,i+2,i+3,0,1)'.format(calc_range_Entry_tuple[i]))
        #Qrange
        calc_qrange_Entry_tuple = ('q_max','hstep','kstep','l_index')
        calc_qrange_Table = gtk.Table(1,5,homogeneous = False)
        self.calc_qrange_Button = gtk.Button('Qrange')
        tooltips.set_tip(self.calc_qrange_Button,'Calculate f\'s within q-max')
        self.calc_qrange_Button.connect('clicked',cyrod_calc.cal_qrange,self)
        self.calc_qrange_Button.set_size_request(40,25)
        calc_qrange_Table.attach(self.calc_qrange_Button,0,1,0,1)
        for i in range(4):
            exec('self.calc_qrange_{0}_Entry = gtk.Entry()'.format(calc_qrange_Entry_tuple[i]))
            exec('self.calc_qrange_{0}_Entry.set_size_request(40,25)'.format(calc_qrange_Entry_tuple[i]))
            exec('self.calc_qrange_{0}_Entry.set_text(\'0.00\')'.format(calc_qrange_Entry_tuple[i]))
            exec('tooltips.set_tip(self.calc_qrange_{0}_Entry,\'{0}\')'.format(calc_qrange_Entry_tuple[i]))
            exec('calc_qrange_Table.attach(self.calc_qrange_{0}_Entry,i+1,i+2,0,1)'.format(calc_qrange_Entry_tuple[i]))
        calc_VBox = gtk.VBox(homogeneous = False,spacing = 1)
        calc_VBox.pack_start(calc_rod_HBox,False,False,0)
        calc_VBox.pack_start(calc_range_Table,False,False,0)
        calc_VBox.pack_start(calc_qrange_Table,False,False,0)
        calc_frame = gtk.Frame(' CALC ')
        tooltips.set_tip(calc_frame,'Calculate structure factors')
        calc_frame.add(calc_VBox)
        
############################################# COMMAND ENTRY AND HELPER ##################################################
        self.CommandEntry = gtk.Entry()
        self.CommandEntry.connect("activate", self.command_callback)
        self.CommandEntry.connect("changed", cyrod_menu.command_helper,self)
        CommandEnterButton = gtk.Button("<")
        CommandEnterButton.connect("clicked", self.command_callback)
        CommandEnterButton.set_size_request(20, 10)
        CommandEntry_HBox = gtk.HBox(homogeneous = False, spacing = 3)
        CommandEntry_HBox.set_border_width(0)
        CommandEntry_HBox.pack_start(self.CommandEntry, True, True, 0)        
        CommandEntry_HBox.pack_start(CommandEnterButton, False, False, 0) 
        
        CommandHelper_ScrolledWindow = gtk.ScrolledWindow()
        CommandHelper_ScrolledWindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        CommandHelper_ScrolledWindow.set_size_request(400,350)
        self.CommandHelper_VBox = gtk.VBox(homogeneous = False, spacing = 0)
        self.CommandHelper_VBox.set_border_width(0)
        CommandHelper_ScrolledWindow.add_with_viewport(self.CommandHelper_VBox)
        self.CommandHelper_VBox.hide()
        cyrod_menu.display_menu(self,cyrod_menu.get_menu('main_menu'))
        self.cmd_flag = "main"
        
        self.Command_VBox = gtk.VBox(homogeneous = False, spacing = 3)
        self.Command_VBox.set_border_width(0)
        self.Command_VBox.pack_start(CommandEntry_HBox, False, False, 0)
        self.Command_VBox.pack_start(CommandHelper_ScrolledWindow, False, False, 0)     

############################################# ITEMFACTORY ##################################################
        self.menu_items = (
            # MenuPath, accelerator key, call back, call back action, item type
            # item type can be : NULL -> "<Item>" , "" -> "<Item>", 
            # "<Title>" -> create a title item, "<Item>" -> create a simple item,
            # "<CheckItem>" -> create a check item,"<ToggleItem>" -> create a toggle item
            # "<RadioItem>" -> create a radio item, <path> -> path of a radio item to link against
            # "<Separator>" -> create a separator, "<Branch>" -> create an item to hold sub items (optional)
            # "<LastBranch>" -> create a right justified branch, "<tearoff>" a tearoff menu

            ( "/_File", None, None, 0, "<Branch>"),
            ( "/_File/_Bul", "<ctrl>B", self.Table_Visibility_Toggled, 1, None),
            ( "/_File/_Sur", "<ctrl>S", self.Table_Visibility_Toggled, 2, None),
            ( "/_File/_Fit", "<ctrl>F", self.Table_Visibility_Toggled, 3, None),
            ( "/_File/_Par", "<ctrl>P", self.Table_Visibility_Toggled, 4, None),
            ( "/_File/_Dat", "<ctrl>D", self.Table_Visibility_Toggled, 5, None),
            ( "/_File/Quit", "<alt>F4", self.MainWindow_Destroy, 0, None),
            ( "/_Process", None, None, 0, "<Branch>"),
            ( "/Process/_Set", "F2", self.Set_Visibility_Toggled, 0, None),
            ( "/Process/_Calc", "F3",None, 0, None),
            ( "/Process/_Fit", "F4", self.FIT_run, 0, None),
            ( "/Process/_Plot", "F5", None, 0, None),
            ( "/_View", None, None, 0, "<Branch>"),
            ( "/_View/Classic", "<shift>C", self.Ter_Visibility_Toggled, 1, "<RadioItem>"),
            ( "/_View/Modern", "<shift>M", self.Ter_Visibility_Toggled, 2, "/View/Classic"),
            ( "/_Help", None, None, 0, "<LastBranch>"),
            ( "/Help/About _CyRod", "F1", None, 0, None),
        )
        # Main window and main menu
        self.Main_Window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        Main_Menu = self.Menu_ItemFactory(self.Main_Window) 

############################################# MAIN VBOX ##################################################
        # LV1
        self.Main_sub_VBox = gtk.VBox(homogeneous = False, spacing = 3) #  .hide() when classic mode called
        self.Main_sub_VBox.set_border_width(3)
        self.Main_sub_VBox.pack_start(Status_Bar_HBox, False, False, 0)
        self.Main_sub_VBox.pack_start(Status_Bar_1_HBox, False, False, 0)
        self.Main_sub_VBox.pack_start(Fit_frame,False,False,0)
        self.Main_sub_VBox.pack_start(calc_frame,False,False,0)
        
        Main_VBox = gtk.VBox(homogeneous = False, spacing = 3)
        Main_VBox.pack_start(Main_Menu, False, False, 0)
        Main_VBox.pack_start(self.Main_sub_VBox, False, False, 0)
        Main_VBox.pack_start(self.Command_VBox, True, True, 0)
        Main_VBox.pack_start(self.Status_Bar_Label, False, False, 0)
        
        # Main Window
        self.Main_Window.connect("destroy", self.MainWindow_Destroy)
        self.Main_Window.set_title("CYROD")
        self.Main_Window.set_position(gtk.WIN_POS_CENTER)
        self.Main_Window.add(Main_VBox)
        self.Main_Window.set_size_request(435,270)
        self.Main_Window.show_all()
        self.Command_VBox.hide()



def main():
    
    settings = gtk.settings_get_default()
    gtk.main()
    return 0



if __name__ == "__main__" or __name__ == "rod":
 

    MyMainWindow()
    initialize()
    main()
    
