# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
import os
import shutil
import re

def PXD_Generator(fin,fout,debug_flag = None):
    newline = fin.readline()
    count = 0
    func_flag = False
    struct_flag = False
    variable_list = ['FILENAME_MAX']
    FILENAME_MAX = 20
    infoflag = False
    while newline != "":
###################################### STRUCT(extra lines) ##############################################
        final_one = ''
        variable_name = ''
        if struct_flag == True:
            if not '}' in newline:
                if '{' in newline:
                    newline = fin.readline()
                    continue
                writeline = '    ' + newline
                writeline = re.findall('(.*);',writeline)[0]
                if '[' in newline:
                    e1 = writeline.split() 
                    final_one = e1[1].split('[')
                    variable_name = final_one[1].rstrip(']')
                    exec('final_one[1] = {0}'.format(variable_name))
                    writeline = '        {0} {1}[{2}]'.format(e1[0],final_one[0],final_one[1])
                fout.write('#' + writeline +'\n')
                newline = fin.readline()
                continue
            else:
                e1 = newline.split()
                if len(e1) == 1 and e1[0].rstrip(';\n') == '}':
                    newline = fin.readline()
                    if newline == '\n':
                        struct_flag = False
                        continue
                    e1 = newline.split()
                    final_one = e1[0]
                elif len(e1) == 2:
                    final_one = e1[1]
                variable_name = final_one.rstrip(';\n').split('[')[1].rstrip(']')
                final_one = final_one.split('[')
                exec('final_one[1] = {0}'.format(variable_name))
                writeline = '        {0}[{1}]'.format(final_one[0],final_one[1])
                fout.write('#' + writeline + '\n')
                struct_flag = False
                newline = fin.readline()
                continue
###################################### functions(extra lines) ##############################################
        if func_flag == True:
            if not ';' in newline:
                writeline = newline
                fout.write('    ' + writeline)
                newline = fin.readline()
                continue
            else:
                writeline = newline[:-1]
                fout.write('    ' + writeline + '\n')
                func_flag = False
                newline = fin.readline()
                continue
        if count < 119:  # to skip the commented lines at the beginning
            count += 1
            continue
        if newline.rstrip(os.linesep)[-2:] == "/*":
            infoflag = True
        if infoflag == True:
            if '*/' not in newline:
                newline = fin.readline()
                continue
            else:
                infoflag = False
                newline = fin.readline()
                continue
        e1 = newline.split()
        if len(e1):
            if e1[0] not in ('*','/*','//','*/') or e1[0][:2] not in ('/*') \
                and not e1[0].startswith('/****') and '*/' not in e1:
                if e1[0] == "#define" and len(e1) > 2:
                    if e1[2] != 'extern' and e1[2].isdigit():
                        exec('{0} = {1}'.format(e1[1],e1[2]))
                        variable_list.append(e1[1])
###################################### extern ##############################################
                elif e1[0] in ("ROD", "CALC", "FIT", "SET", "PLOT", "KEATING",'extern'): #DONE
                    try:
                        e1[2] = e1[2].rstrip(';')
                        e2 =  e1[2].split('[')
                        for i in range(len(e2)):
                            if ']' in e2[i]:
                                e2[i] = e2[i].rstrip(']')
                            if e2[i].endswith('];/*'):
                                e2[i] = e2[i].rstrip('];/*')
                            if e2[i] in variable_list:
                                exec('e2[i] = {0}'.format(e2[i]))
                        for i in range(len(e2)):
                            e2[i] = str(e2[i])
                            if i >= 1:
                                e2[i] = '[' + e2[i] + ']'
                        if len(e2) > 1:
                            writeline = '    {0} {1}'.format(e1[1],''.join(e2))
                        elif e1[1] == 'unsigned':
                            writeline = '    {0} {1} {2}'.format(e1[1],e2[0],e1[3].rstrip(';'))
                        else:
                            writeline = '    {0} {1}'.format(e1[1],e2[0])
                        if e1[1] != 'MATRIX':  #MATRIX DO IT LATER not done yet
                            fout.write(writeline + '\n')
###################################### struct ##############################################
                    except:
                        if e1[1] == 'struct':  # NOT DONE YET
                            struct_flag = True
                            writeline = '    ' + newline
                            fout.write('#' + writeline)
                            newline = fin.readline()
                            continue
###################################### LFLAGS FUNC ##############################################
                else:
                    if e1[0] == 'LFLAGS':#done
                        writeline = '    {0} {1}'.format(e1[1],' '.join(e1[2:]))
                        #print newline.rstrip()
                        if writeline.endswith(';'):
                            writeline =  writeline[:-1]
                        if not writeline.endswith(')'):
                            func_flag = True
                            fout.write(writeline + '\n')
                            newline = fin.readline()
                            continue
                        else:
                            writeline = ' '.join(newline.rstrip(';\n').split(' ')[1:])
                            fout.write('    ' + writeline + '\n')
###################################### general func ##############################################
                    elif e1[0] in ('void','int','float','double','char','struct'):# done
                        if e1[0] == 'struct':
                            writeline = '#    {0} {1}'.format(e1[0],e1[1])
                            fout.write(writeline + '\n')
                            newline = fin.readline()
                            struct_flag = True
                            continue
                    
                        if '(' not in e1[1]:
                            writeline = '    {0} {1}'.format(e1[0],e1[1].rstrip(';'))
                            e1 = writeline.split('[')
                            if len(e1) >= 2:
                                variable_name = e1[1].rstrip(']')
                                exec('e1[1] = {0}'.format(variable_name))
                                if len(e1) == 3:
                                    e1[2] = e1[2].rstrip(']')
                                    writeline = '{0}[{1}][{2}]'.format(e1[0],e1[1],e1[2])
                                elif len(e1) == 2:
                                    writeline = '{0}[{1}]'.format(e1[0],e1[1])
                            else:
                                writeline = e1[0]
                            fout.write(writeline + '\n')
                            
                        else: #other functions
                            if ';' not in newline:
                                func_flag = True
                                fout.write('    ' + newline.rstrip(';\n') + '\n')
                                newline = fin.readline()
                                continue
                            writeline = '    ' + newline.rstrip(';\n')
                            fout.write(writeline + '\n')
                            
                    elif e1[0] == 'typedef':  
                        writeline = '    ' + newline.rstrip(';\n')
                        fout.write(writeline + '\n')
                            
                    elif '*/' in e1 or e1[0].startswith('//') or e1[0].startswith('/*') or e1[0] in \
                        ('#if','#define','#ifdef','#include','#ifndef','#undef','Update') or len(e1) == 1\
                        or e1[0][0].isdigit():
                        pass
                    else:
                        pass
            else:
                pass
        newline = fin.readline()
    fout.close()
    fin.close()


#fin = open("lsqfit.h", "r")
#fout = open("rod2.pxd", "w")
#fout.write('# redefining the C API in lsqfit.h\n')
#fout.write('cdef extern from "lsqfit.h":\n')
#PXD_Generator(fin,fout,'menu')

fin = open("./src/include/menu.h", "r")
fout = open("./cyrod/rod2.pxd", "w")
fout.write('# redefining the C API in menu.h\n')
fout.write('cdef extern from "menu.h":\n')
PXD_Generator(fin,fout,'menu')

fin = open("./src/rod/rod.h", "r")
fout = open("./cyrod/rod2.pxd", "a")
fout.write('# redefining the C API in rod.h\n')
fout.write('cdef extern from "rod.h":\n')
PXD_Generator(fin,fout,'rod')

# However some of the forms of the functions are not accepted by pxd files
# Thus some modifications need to be done here
fin = open('./cyrod/rod2.pxd','r')
fout = open('./cyrod/rod.pxd','w')
fout.write('# -*- coding: utf-8 -*-\n')

fout.write('####################################################################################\n')
fout.write('####    CyRod - Python Wrapper for rod                                          ####\n') 
fout.write('####    Email to nrszhou@gmail.com for any questions or comments                ####\n')
fout.write('####    Version : 1.0.0 (Feb. 2016)                                             ####\n')
fout.write('####    Project site: wait....                                                  ####\n')
fout.write('####################################################################################\n')
fout.write('# Those who are not recognized by Cython are commented\n\n\n\n')


fout.write('# redefining the C API in lsqfit.h\n')
fout.write('cdef extern from \"lsqfit.h\":\n')
fout.write('    float gammq(float,float)\n')

newline = fin.readline()
while newline != '':
    
    if '(void)' in newline:
        newline = newline.replace('(void)','()')
    if 'struc' in newline or 'jmp_buf' in newline or 'typedef' in newline\
        or 'MATRIX' in newline:
        writeline = '#'+newline.rstrip()
        if '(' in newline and ')' not in newline:
            newline = fin.readline()
            writeline = writeline + newline.rstrip()
    else:
        writeline = newline.rstrip()
    writeline = writeline.rstrip(';')
    newline = fin.readline()
    fout.write(writeline + '\n')
fin.close()
fout.close()
os.remove('./cyrod/rod2.pxd')


#patch menu.c
os.rename('./src/menu/menu.c','./src/menu/menu1.c')
fin = open('./src/menu/menu1.c','r')
fout = open('./src/menu/menu.c','w')

newline = fin.readline()
if newline.startswith('//patch v0.0.1\n'):
    patch_flag = False
else:
    patch_flag = True
    fout.write('//patch v0.0.1\n')
type_line_flag = False
init_menu_flag = False
count_define = 0
define_flag = False
if patch_flag == False:
    while newline != '':
        fout.write(newline)
        newline = fin.readline()
while newline != '' and patch_flag == True:
    if newline.startswith('#include <menu.h>'):
        fout.write(newline)
        fout.write('#define CYROD\n')
        newline = fin.readline()
        continue
    elif newline.startswith(' * Get number of rows on screen'):
        newline = fin.readline()
        while newline != '':
            if '{' in newline:
                fout.write(newline)
                break
            fout.write(newline)
            newline = fin.readline()
        fout.write('    return 9999;\n')
        newline = fin.readline()
        continue
    elif newline.startswith('int init_menu(void)'):
        init_menu_flag = True
    elif newline.startswith('void type_line(const char tline[])'):
        type_line_flag = True
    if newline.startswith('#if !defined(_MSC_VER) && !defined(READLINE)') and type_line_flag == True:
        newline = newline.rstrip() + ' && !defined(CYROD)\n'
        type_line_flag = False
    if newline.startswith('#if defined(TERMBUFFER) && !defined(_MSC_VER)') and init_menu_flag == True:
        newline = newline.rstrip() + ' && !defined(CYROD)\n'
        init_menu_flag = False
    fout.write(newline)
    newline = fin.readline()
    if ' && !defined(CYROD)\n' in newline:
        print newline


fin.close()
fout.close()
os.remove('./src/menu/menu1.c')

# patch rod.h
# #define ALLOW_PLOT commented
os.rename('./src/rod/rod.h','./src/rod/rod1.h')
fin = open('./src/rod/rod1.h','r')
fout = open('./src/rod/rod.h','w')

newline = fin.readline()
if newline.startswith('//patch v0.0.1\n'):
    patch_flag = False
else:
    patch_flag = True
    fout.write('//patch v0.0.1\n')
if patch_flag == False:
    while newline != '':
        fout.write(newline)
        newline = fin.readline()
while newline != '' and patch_flag == True:
    if newline.startswith('#define ALLOW_PLOT'):
        newline = '//'+newline
    fout.write(newline)
    newline = fin.readline()

fin.close()
fout.close()
os.remove('./src/rod/rod1.h')

try:
    shutil.copyfile('./numrec.o','./src/lsqfit/numrec.o')
except:
    pass
try:
    os.remove('./numrec.o')
except:
    pass
try:
    os.remove('./src/lsqfit/numrec.c')
except:
    pass

rod_source_dir = ["lsqfit", "menu", "rod"]
rod_source = []
for path in rod_source_dir:
    for file in os.listdir(os.path.join('src', path)):
        if file[-1] == 'c':
            rod_source += [os.path.join('src' ,path, file)]



include_dirs = [os.path.join('src', 'include'),\
                os.path.join('src', 'plot'),\
                os.path.join('src', 'rod')]

setup(
    name = "cyrod", version = '1.0', description = 'Python Wrapper for ROD',
    ext_modules = cythonize([
        Extension('rod', [os.path.join('cyrod', 'rod.pyx')]+ rod_source,
                  include_dirs = include_dirs, extra_objects=["./src/lsqfit/numrec.o"], 
                  libraries=['ncurses'], extra_compile_args=['-w']
        ),
        Extension('cyrod_read', [os.path.join('cyrod', 'cyrod_read.py')], extra_compile_args=['-w']),
        Extension('cyrod_plot', [os.path.join('cyrod', 'cyrod_plot.py')], extra_compile_args=['-w']),
        Extension('cyrod_set', [os.path.join('cyrod', 'cyrod_set.py')], extra_compile_args=['-w']),
        Extension('cyrod_calc', [os.path.join('cyrod', 'cyrod_calc.py')], extra_compile_args=['-w']),
        Extension('cyrod_menu', [os.path.join('cyrod', 'cyrod_menu.py')], extra_compile_args=['-w'])
    ]),
)

# this is for test
'''shutil.copyfile('./66.dat', './build/lib.linux-x86_64-2.7/66.dat')
shutil.copyfile('./66new.fit', './build/lib.linux-x86_64-2.7/66.fit')
shutil.copyfile('./66.par', './build/lib.linux-x86_64-2.7/66.par')
shutil.copyfile('./macro.mac', './build/lib.linux-x86_64-2.7/macro.mac')
shutil.copyfile('./66.bul', './build/lib.linux-x86_64-2.7/66.bul')
shutil.copyfile('./66.bul', './build/lib.linux-x86_64-2.7/66.sur')
shutil.copyfile('./script.sh', './build/lib.linux-x86_64-2.7/script.sh')
shutil.copyfile('./log.txt', './build/lib.linux-x86_64-2.7/slot.txt')
shutil.copyfile('./77.bul', './build/lib.linux-x86_64-2.7/77.bul')
shutil.copyfile('./77.sur', './build/lib.linux-x86_64-2.7/77.sur')
shutil.copyfile('./77.fit', './build/lib.linux-x86_64-2.7/77.fit')
shutil.copyfile('./77.dat', './build/lib.linux-x86_64-2.7/77.dat')
shutil.copyfile('./77.par', './build/lib.linux-x86_64-2.7/77.par')
shutil.copyfile('./77mac.mac', './build/lib.linux-x86_64-2.7/77mac.mac')'''
